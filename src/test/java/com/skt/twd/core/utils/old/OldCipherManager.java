package com.skt.twd.core.utils.old;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cubeone.CubeOneAPI;
import com.skt.twd.webt.AppConfig;

import cryptix.provider.key.RawSecretKey;
import cryptix.util.core.Hex;
import xjava.security.Cipher;

public class OldCipherManager {

	private static Logger log = LoggerFactory.getLogger(OldCipherManager.class);

	public static OldCipherManager instance;
	private static AppConfig appConfig;
	private final static String mKey = "QWERTYUIOPASDFGHJKLZXCV2"; // 24byte

	/**
	 * Tworld 암호화 Key
	 */
    private final static String MKEY_TPOINT	= "203030303032333230305892"; // T포인트 제휴 암호화 키 (20151001)


	public static void setAppConfig(AppConfig appConf) {
		appConfig = appConf;
	}

	public static OldCipherManager getInstance() {
		if (instance == null)
			instance = new OldCipherManager();
		return instance;
	}

	/**
    *
    * <pre>
    * HEX Type 데이터를 암호화 된 데이터로 변환
    * </pre>
    *
    * @param input
    * @param inKey
    * @return
    */
   public  String encrypt(String input, String inKey) {
       byte[] ect = null;
       String message = "";

       try {
           message = new String(input.getBytes("euc-kr"),"8859_1");
           /***** 메세지 길이 체크(8의 배수여야 한다) *****/
           while( (message.length() % 8) != 0 ) message += " ";

           Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
           RawSecretKey key = new RawSecretKey("DES_EDE3", inKey.getBytes());

           alg.initEncrypt(key);
           ect = alg.crypt(message.getBytes("8859_1"));
			if ( log.isDebugEnabled() ) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + ect);
			}
       } catch ( Exception e ) {
				log.error(e.getMessage(), e);
       }
       return Hex.toString(ect);
   }

   /**
    *
    * <pre>
    * 암호화 된 데이터를 원래의 HEX Type으로 복호화
    * </pre>
    *
    * @param input
    * @param inKey
    * @return
    */
   public  String decrypt(String input, String inKey) {
       byte[] dct = null;

       try {
           Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
           RawSecretKey key = new RawSecretKey("DES_EDE3", inKey.getBytes());

           alg.initDecrypt(key);
           dct = alg.crypt(Hex.fromString(input));

			if ( log.isDebugEnabled() ) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + dct);
			}


       } catch ( Exception e ) {
				log.error(e.getMessage(), e);
       }
       return new String(dct).trim();
   }

	/*---------------------------------------------------------
	  HEX Type 데이터를 암호화 된 데이터로 변환
	----------------------------------------------------------*/
	public String encrypt(String input) {
		String ect = "";

		try {
			input = (input == null) ? "" : input.trim(); //StringUtil.strNull(input);
//        	if(true){return input;}
			if ("".equals(input)) {
				return input;
			}

			if(appConfig.isLocal()) {
				ect = input;
			}else {
				ect = CubeOneAPI.jcoencrypt(input);
			}

			// System.out.println(input + " : " + ect);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ect.trim();
	}

	/*--------------------------------------------------------
	  암호화 된 데이터를 원래의 HEX Type으로 복호화
	--------------------------------------------------------*/
	public String decrypt(String input) {
		String dct = "";
		try {
			input = (input == null) ? "" : input.trim(); //StringUtil.strNull(input);
//        	if(true){return input;}
			if ("".equals(input)) {
				return input;
			}

			if(appConfig.isLocal()) {
				dct = input;
			} else {
				dct = CubeOneAPI.jcodecrypt(input);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dct;
	}

	public static String getMkeyTpoint() {
		return MKEY_TPOINT;
	}
}
