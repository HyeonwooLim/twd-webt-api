package com.skt.twd.core.utils.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TestVo {
	private int queryPageNo;
	private String queryStartTime;
	private String queryEndTime;
}
