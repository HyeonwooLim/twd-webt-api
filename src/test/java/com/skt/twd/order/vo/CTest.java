package com.skt.twd.order.vo;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CTest {
	private String memo;
	private List<ItemTestVo> list;
}
