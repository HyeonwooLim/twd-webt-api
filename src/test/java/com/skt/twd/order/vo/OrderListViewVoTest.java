package com.skt.twd.order.vo;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "주문 목록 View 사용되는 VO 이다.")
public class OrderListViewVoTest {
	private int id;
	private String userName;
	private String userId;
	private String address;
	private String phoneNumber;
	private int price;
	private String memo;
	private String status;
	private Date regDate;
	private Date updDate;
	private int orderItemId;
	private String productId;
	private String addServiceId;
}
