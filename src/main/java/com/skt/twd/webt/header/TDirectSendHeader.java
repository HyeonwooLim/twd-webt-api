package com.skt.twd.webt.header;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.processor.annotation.FieldItem;

@JsonIgnoreType
public class TDirectSendHeader implements IHeaderField{
	/** Stream Length */
	@FieldItem(order=0, length=8, remarks="StreamLength")
	public String totalLength;
	/** GlobalID */
	@FieldItem(order=1, length=32, remarks="GlobalID")
	public String global_id;
	/** 거래코드 */
	@FieldItem(order=2, length=24, remarks="거래코드")
	public String tx_code;
	/** 요청응답구분 */
	@FieldItem(order=3, length=1, remarks="요청응답구분", value="S")
	public String send_rspn_type;
	/** 처리결과 */
	@FieldItem(order=4, length=1, remarks="처리결과")
	public String err_flag;
	/** 오류코드 */
	@FieldItem(order=5, length=9, remarks="오류코드")
	public String err_code;
	/** 오류메시지 */
	@FieldItem(order=6, length=80, remarks="오류메시지")
	public String err_msg;
	/** 종별코드 */
	@FieldItem(order=7, length=4, remarks="종별코드", value="0000")
	public String kind_code;
	/** 채널코드 */
	@FieldItem(order=8, length=10, remarks="채널코드")
	public String channel_id;
	/** 주운영시스템상태 */
	@FieldItem(order=9, length=1, remarks="주운영시스템상태")
	public String st_stop;
	/** 회사구분코드 */
	@FieldItem(order=10, length=1, remarks="회사구분코드")
	public String ngms_coclcd;
	/** 임시필러 */
	@FieldItem(order=11, length=18, remarks="임시필러")
	public String reserved;
	/** MSG카운트 */
	@FieldItem(order=12, length=1, remarks="MSG카운트", value="0")
	public String msg_cnt;
	
	@Override
	public String toString() {
		return "TDirectSendHeader [totalLength=" + totalLength + ", global_id=" + global_id + ", tx_code=" + tx_code
				+ ", send_rspn_type=" + send_rspn_type + ", err_flag=" + err_flag + ", err_code=" + err_code
				+ ", err_msg=" + err_msg + ", kind_code=" + kind_code + ", channel_id=" + channel_id + ", st_stop="
				+ st_stop + ", ngms_coclcd=" + ngms_coclcd + ", reserved=" + reserved + ", msg_cnt=" + msg_cnt + "]";
	}
}
