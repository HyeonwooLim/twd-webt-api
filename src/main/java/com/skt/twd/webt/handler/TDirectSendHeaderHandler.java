package com.skt.twd.webt.handler;

import com.skt.twd.webt.header.TDirectSendHeader;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.handler.ISendHeaderHandler;
import com.skt.twd.webt.integration.service.WebTServiceInfo;

public class TDirectSendHeaderHandler implements ISendHeaderHandler{

	@Override
	public void handleHeader(IHeaderField header, WebTServiceInfo info, WebTProperites properties) {
		TDirectSendHeader headerVO = (TDirectSendHeader)header;
		headerVO.tx_code = info.getTxCode();
		headerVO.channel_id = info.getChannelId() != null
				? info.getChannelId()
				: properties.getChannelPropety(info.getChannelName()).getChannelId();

	}
}
