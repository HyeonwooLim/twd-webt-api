package com.skt.twd.webt;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.skt.twd.webt.config.FrontTypeProperties;
import com.skt.twd.webt.integration.service.IWebTService;
import com.skt.twd.webt.integration.service.WebTServiceInfo;
import com.skt.twd.webt.utils.exceptions.InterfaceClientException;
import com.skt.twd.webt.utils.exceptions.InterfaceClientException.InterfaceType;

/**
 * WebT, legacy 채널 연동처리 delegator
 * @author ahnhojung
 *
 */
@Component
public class LegacyInterfaceDelegator {

	@Autowired
	private FrontTypeProperties properties;

	@Autowired
	private IWebTService service;

	/** 멤버채널ID */
	public static final String HTTP_HEADER_KEY_MBR_CHANNEL_ID = "TWD-MbrChlId";
	
	/** 입력 채널의 Channel Name 을 전달하기 위한 key */
	public static final String HTTP_HEADER_KEY_CHANNEL_NAME = "TWD-ChannelName";
    
	/**
	 * front channel별 처리를 위해 request header에 입력된 bff-name정보를 조회
	 * @return bffname bff name
	 */
	private String getBffName() {
		ServletRequestAttributes servletRequest = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(servletRequest != null) {
			return StringUtils.defaultString(servletRequest.getRequest().getHeader(
					HTTP_HEADER_KEY_CHANNEL_NAME),
					"twd-bff-web");
		}else{
			return "twd-bff-web";
		}
	}

	/**
	 * application.yml에 정의된 bff name 정보가 있는지 조회하여 등록된 bff-name이 없는 경우 exception 발생
	 * @param bffName
	 * @throws InterfaceClientException
	 */
	private void findFrontTypeName(final String bffName) throws InterfaceClientException{
		if(!properties.getFrontTypeName().containsKey(bffName)) {
			InterfaceClientException exception = new InterfaceClientException("INF9001", InterfaceType.SWING);
			exception.setDebugMessage("front bff type name not exist [" + bffName + "]");
			throw exception;
		}
	}

	/**
	 * webt service execute
	 * @param txcode txcode
	 * @param in webt in-vo
	 * @param out webt out-vo class
	 * @return webt out-vo
	 * @throws InterfaceClientException
	 */
	public <T> T callWebTService(final String txcode, final Object in, final Class<T> out) throws InterfaceClientException{
		return callWebTService(txcode, in, out, WebTChannelName.DEFAULT);
	}

	/**
	 * webt service execute
	 * @param txcode txcode
	 * @param in webt in-vo
	 * @param out webt out-vo class
	 * @param channelName webt channel name
	 * @return webt out-vo
	 * @throws InterfaceClientException
	 */
	public <T> T callWebTService(final String txcode, final Object in, final Class<T> out, final WebTChannelName channelName) throws InterfaceClientException{
		String bffName = getBffName();
		findFrontTypeName(bffName);

		String poolGroupName = Optional.ofNullable(properties.getMediaType(bffName)
				.getWebTChannel(channelName.getChannelName()))
				.orElseThrow(()->new InterfaceClientException("INF9001", InterfaceType.SWING, "webt channel name not defined [" + channelName + "]"))
				.getPoolGroupName();

		String channelId = Optional.ofNullable(properties.getMediaType(bffName)
				.getWebTChannel(channelName.getChannelName()))
				.orElseThrow(()->new InterfaceClientException("INF9001", InterfaceType.SWING, "webt channel name not defined [" + channelName + "]"))
				.getChannelId();

		String serviceId = Optional.ofNullable(properties.getMediaType(bffName)
				.getWebTChannel(channelName.getChannelName()))
				.orElseThrow(()->new InterfaceClientException("INF9001", InterfaceType.SWING, "webt channel name not defined [" + channelName + "]"))
				.getServiceId();

		WebTServiceInfo serviceInfo = WebTServiceInfo.builder(channelName.getChannelName())
				.setPoolGroupName(poolGroupName)
				.setServiceId(serviceId)
				.setChannelId(channelId)
				.setTxCode(txcode)
				.build();
		return service.service(serviceInfo, in, out);
	}
}
