package com.skt.twd.webt.proxy;

/**
 * proxy 사용시 거래 데이터 정보저장 DTO
 *
 * @author ahnhojung
 *
 */
public class WebTProxyDTO {
	private String groupName;
	private String serviceId;
	private String errorCode;
	private String errorMessage;
	private byte[] message;

	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public byte[] getMessage() {
		return message;
	}
	public void setMessage(byte[] message) {
		this.message = message;
	}
}
