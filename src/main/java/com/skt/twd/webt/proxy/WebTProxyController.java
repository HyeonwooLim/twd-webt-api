package com.skt.twd.webt.proxy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.skt.twd.webt.integration.common.WebTBanner;
import com.skt.twd.webt.integration.common.WebTConst;
import com.skt.twd.webt.integration.service.IWebTService;
import com.skt.twd.webt.integration.service.WebTServiceBean;

import lombok.extern.slf4j.Slf4j;

/**
 * Proxy 사용 시 전문 수신을 위한 controller 클래스
 *
 * @author ahnhojung
 *
 */
@RestController
@Slf4j
public class WebTProxyController implements InitializingBean{

	@Autowired
	private IWebTService service;

	@GetMapping(WebTConst.WEBT_DEFAULT_PROXY_URL+"/test")
	public String testUrl() {
		return "test ok";
	}

	@PostMapping(WebTConst.WEBT_DEFAULT_PROXY_URL)
	public @ResponseBody WebTProxyDTO proxyService(@RequestBody WebTProxyDTO dto){
		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		try {
			bout.write(dto.getMessage());
			log.debug("MCGWebTProxyController RECV. groupName : {}, serviceId : {}, message {}", dto.getGroupName(), dto.getServiceId(), new String(dto.getMessage()));
			ByteArrayInputStream bin = ((WebTServiceBean)service).tpcall(dto.getGroupName(), dto.getServiceId(), bout);
			byte [] buf = new byte[bin.available()];
			bin.read(buf);
			dto.setMessage(buf);
			log.debug("MCGWebTProxyController SEND. groupName : {}, serviceId : {}, message {}", dto.getGroupName(), dto.getServiceId(), new String(dto.getMessage()));
		}catch(Exception e) {
			dto.setErrorCode("MCGWebTProxyController error");
			dto.setErrorMessage(e.getMessage());
		}

		return dto;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info(WebTBanner.getAnsiColor("[WebTService] WebTProxy rest service start...", WebTBanner.AnsiColor.ANSI_CYAN));
	}
}
