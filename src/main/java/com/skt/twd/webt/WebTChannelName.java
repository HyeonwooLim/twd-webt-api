package com.skt.twd.webt;

public enum WebTChannelName {
	/*
	default: &default
      pool-group-name: mcg
      channel-id: TSMSP
      service-id: M1240_1241
	ocb: &ocb
      pool-group-name: mcg
      channel-id: TSMSP_OCB
      service-id: M1240_1242
    nene: &nene
      pool-group-name: mcg
      channel-id: NENE
      service-id: M1080_1081
    efm: &efm # efm channel
        pool-group-name: mcg
        channel-id: TWD
        service-id: M1240_1243
	 */
	DEFAULT("default"),
	OCB("ocb"),
	NENE("nene"),
	EFM("efm");

	private String channelName;

	WebTChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelName() {
		return channelName;
	}
}
