package com.skt.twd.webt.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skt.twd.webt.handler.TDirectRecvHeaderHandler;
import com.skt.twd.webt.handler.TDirectSendHeaderHandler;
import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.handler.IReceiveHeaderHandler;
import com.skt.twd.webt.integration.handler.ISendHeaderHandler;
import com.skt.twd.webt.integration.logevent.JsonMaskingModule;
import com.skt.twd.webt.integration.logevent.notifier.ByteLogEventNotifier;
import com.skt.twd.webt.integration.logevent.notifier.ObjectLogEventNotifier;
import com.skt.twd.webt.processor.EfmWebTSendProcessor;
import com.skt.twd.webt.processor.OcbWebTRecvProcessor;
import com.skt.twd.webt.processor.OcbWebTSendProcessor;

/**
 * WebT, legacy 채널 configuration
 * @author ahnhojung
 *
 */
@Configuration
@EnableConfigurationProperties(value = { FrontTypeProperties.class})
public class ChannelConfiguration {
	public static final String WEBT_IO_LOGGER_NAME = "webtio";
	public static final String WEBT_JSON_LOGGER_NAME = "webtjson";

	@Bean
	public ISendHeaderHandler defaultSendHeaderHandler() {
		return new TDirectSendHeaderHandler();
	}

	@Bean
	public IReceiveHeaderHandler defaultReceiveHeaderHandler() {
		return new TDirectRecvHeaderHandler();
	}

	/**
	 * WebT ocb 채널 send parser bean
	 * @return ocb send parser
	 */
	@Bean("ocbSendProcessor")
	public OcbWebTSendProcessor ocbSendProcessor() {
		OcbWebTSendProcessor sendProcessor = new OcbWebTSendProcessor();
		sendProcessor.setSendHeaderHandler(defaultSendHeaderHandler());
		return sendProcessor;
	}

	/**
	 * WebT ocb 채널 receive parser bean
	 * @return ocb receive parser
	 */
	@Bean("ocbRecvProcessor")
	public OcbWebTRecvProcessor ocbRecvProcessor() {
		OcbWebTRecvProcessor recvProcessor = new OcbWebTRecvProcessor();
		recvProcessor.setReceiveHeaderHandler(defaultReceiveHeaderHandler());
		return recvProcessor;
	}

	/**
	 * WebT efm 채널 send parser bean (서식지)
	 * @return ocb send parser
	 */
	@Bean("efmSendProcessor")
	public EfmWebTSendProcessor efmSendProcessor() {
		EfmWebTSendProcessor sendProcessor = new EfmWebTSendProcessor();
		sendProcessor.setSendHeaderHandler(defaultSendHeaderHandler());
		return sendProcessor;
	}

	/**
	 * WebT 연동 log 처리 listener component 등록
	 * @author ahnhojung
	 *
	 */
	@Component
	class TDirectWebTLogEventListener{
		private final Logger ioLogger = LoggerFactory.getLogger(ChannelConfiguration.WEBT_IO_LOGGER_NAME);
		private final Logger jsonLogger = LoggerFactory.getLogger(ChannelConfiguration.WEBT_JSON_LOGGER_NAME);

		ObjectMapper mapper = new ObjectMapper();

		@Autowired
		private WebTLogMaskingProperties  properties;

		/**
		 * 송신 webt bytes log 처리
		 * @param logEvent bytes log event
		 */
		@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).SEND_COMPLATE")
		public void sendByteComplate(final ByteLogEventNotifier logEvent) {
			if(ioLogger.isTraceEnabled()) {
				ioLogger.trace("SEND: [{}]", new String(logEvent.getEventSource()));
			}
		}
		/**
		 * 수신 webt bytes log 처리
		 * @param logEvent bytes log event
		 */
		@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).RECV_COMPLATE")
		public void recvByteComplate(final ByteLogEventNotifier logEvent) {
			if(ioLogger.isTraceEnabled()) {
				ioLogger.trace("RECV: [{}]", new String(logEvent.getEventSource()));
			}
		}
		/**
		 * 송신 VO log 처리
		 * @param logEvent vo log event
		 */
		@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).SEND_COMPLATE")
		public void sendObjectComplate(final ObjectLogEventNotifier logEvent) {
			mapper.disable(MapperFeature.USE_ANNOTATIONS);
			mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
			mapper.registerModule(new JsonMaskingModule(properties.getKeyMap(), logEvent.isMasking()));

			try {
				if(jsonLogger.isTraceEnabled()) {
					jsonLogger.trace("SEND: {}" , mapper.writerWithDefaultPrettyPrinter().writeValueAsString(logEvent.getEventSource()));
					// messageLogger.trace("SEND: {}" , mapper.writeValueAsString(logEvent.getEventSource()));
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		/**
		 * 수신 VO log 처리
		 * @param logEvent vo log event
		 */
		@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).RECV_COMPLATE")
		public void recvObjectComplate(final ObjectLogEventNotifier logEvent) {
			mapper.disable(MapperFeature.USE_ANNOTATIONS);
			mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
			mapper.registerModule(new JsonMaskingModule(properties.getKeyMap(), logEvent.isMasking()));
			try {
				if(jsonLogger.isTraceEnabled()) {
					jsonLogger.trace("RECV: {}" , mapper.writerWithDefaultPrettyPrinter().writeValueAsString(logEvent.getEventSource()));
					// messageLogger.trace("RECV: {}" , mapper.writeValueAsString(logEvent.getEventSource()));
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
	}
}
