package com.skt.twd.webt.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.skt.twd.webt.integration.common.WebTProperites.WebTChannelProperty;

@ConfigurationProperties(prefix = "tdirect")
public class FrontTypeProperties {
	/** tworld 입력매체별 legacy channel 정보 */
	private Map <String, TypeProperties> frontTypeName;


	public Map<String, TypeProperties> getFrontTypeName() {
		return frontTypeName;
	}

	public void setFrontTypeName(Map<String, TypeProperties> frontTypeName) {
		this.frontTypeName = frontTypeName;
	}

	public TypeProperties getMediaType(String key) {
		return frontTypeName.get(key);
	}



	public static class TypeProperties{

		/** webt 채널 정보 */
		private Map<String, WebTChannelProperty> webt;


		public Map<String, WebTChannelProperty> getWebt() {
			return webt;
		}
		public void setWebt(Map<String, WebTChannelProperty> webt) {
			this.webt = webt;
		}

		public WebTChannelProperty getWebTChannel(String channelName) {
			return this.webt.get(channelName);
		}
	}

}
