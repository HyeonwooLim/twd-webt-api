package com.skt.twd.webt.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.processor.DefaultWebTSendProcessor;
import com.skt.twd.webt.integration.service.WebTServiceInfo;

import tmax.webt.WebtSystem;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

public class EfmWebTSendProcessor extends DefaultWebTSendProcessor {

    private static Logger logger = LoggerFactory.getLogger(EfmWebTSendProcessor.class);

    @Override
    public ByteArrayOutputStream send(WebTServiceInfo serviceInfo, Object vo) throws WebTServiceException {
        ByteArrayOutputStream bout = super.send(serviceInfo, vo);
        ByteArrayOutputStream addHeaderOut = makeHeader("1240", "1243", "0000", serviceInfo.getTxCode(), bout.toByteArray());
        try {
            logger.debug(new String(addHeaderOut.toByteArray(), WebtSystem.getDefaultCharset()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return addHeaderOut;
    }

    //서식지 추가
    private ByteArrayOutputStream makeHeader(String instCode, String applCode, String kindCode, String txCode, byte[] data) {
        byte[] magic = new byte[4];
        byte[] seqno = new byte[4];
        byte[] reqtype = new byte[4];
        byte[] msgtype = new byte[4];
        byte[] flags = new byte[4];
        byte[] etc = new byte[4];
        byte[] etc2 = new byte[4];
        byte[] inst_code = new byte[4];
        byte[] appl_code = new byte[4];
        byte[] kind_code = new byte[8];
        byte[] tx_code = new byte[32];
        byte[] msg_uid = new byte[10];
        byte[] log_id = new byte[28];
        byte[] resvd = new byte[6];
        ByteArrayOutputStream outStrm = new ByteArrayOutputStream();
        try {
            magic = htonl(5555);
            outStrm.write(magic);
            seqno = htonl(0);
            outStrm.write(seqno);
            reqtype = htonl(11);
            outStrm.write(reqtype);
            msgtype = htonl(101);
            outStrm.write(msgtype);
            flags = htonl(8);
            outStrm.write(flags);
            etc = htonl(0);
            outStrm.write(etc);
            etc2 = htonl(0);
            outStrm.write(etc2);
            inst_code = addSpace(instCode, instCode.length(), 4).getBytes();
            outStrm.write(inst_code);
            appl_code = addSpace(applCode, applCode.length(), 4).getBytes();
            outStrm.write(appl_code);
            kind_code = addSpace(kindCode, kindCode.length(), 8).getBytes();
            outStrm.write(kind_code);
            tx_code = addSpace(txCode, txCode.length(), 32).getBytes();
            outStrm.write(tx_code);
            msg_uid = addSpace(" ", 1, 10).getBytes();
            outStrm.write(msg_uid);
            log_id = addSpace(" ", 1, 28).getBytes();
            outStrm.write(log_id);
            resvd = addSpace(" ", 1, 6).getBytes();
            outStrm.write(resvd);
            outStrm.write(data);
        } catch (Exception e) {
            logger.error(e.toString());
        }

        return outStrm;
    }

    private String addSpace(String arg, int size, int totSize) {
        if (arg.length() == totSize)
            return arg;
        while (arg.length() < totSize) {
            arg += " ";
        }
        byte[] tmp = arg.getBytes();
        tmp[size] = 0x00;
        arg = new String(tmp);
        return arg;
    }

    private byte[] htonl(int arg) {
        byte[] bigEndian = new byte[4];
        bigEndian[0] = (byte) ((arg & 0xFF000000) >> 24);
        bigEndian[1] = (byte) ((arg & 0x00FF0000) >> 16);
        bigEndian[2] = (byte) ((arg & 0x0000FF00) >> 8);
        bigEndian[3] = (byte) (arg & 0x000000FF);
        return bigEndian;
    }
}
