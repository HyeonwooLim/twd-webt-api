package com.skt.twd.webt.processor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.logevent.MaskingInfo;
import com.skt.twd.webt.integration.processor.DefaultWebTSendProcessor;
import com.skt.twd.webt.integration.processor.annotation.FieldItem;
import com.skt.twd.webt.integration.service.WebTServiceUtil;

public class OcbWebTSendProcessor extends DefaultWebTSendProcessor{

	private static Logger logger = LoggerFactory.getLogger(OcbWebTSendProcessor.class);

	@Autowired
	private WebTLogMaskingProperties logmaskingProperties;

	/**
	 * field array의 정보를 이용하여 전송 할 byte[] data stream 생성
	 * @param offset messgae build current offset
	 * @param fields send vo field array
	 * @param vo send vo
	 * @param bout send byte streawm
	 * @param maskingList masking field
	 * @return build offset
	 * @throws WebTServiceException message build exception
	 */
	@SuppressWarnings("rawtypes")
	protected int messageBuild(int offset, Field[] fields, Object vo, ByteArrayOutputStream bout,
			ArrayList<MaskingInfo> maskingList) throws WebTServiceException {

		try {
			// annotation 에 정의된 order로 sort
			fields = WebTServiceUtil.sortOrder(fields);
			// 송신 전문에 record의 count-reference field가 있는 경우 값을 미리 설정
			setRecordFieldCount(fields, vo);

			for(Field field : fields) {
				field.setAccessible(true);
				FieldItem annotation = field.getDeclaredAnnotationsByType(FieldItem.class)[0];

				String fieldName = field.getName();
				int length = annotation.length();
				String cntRef = annotation.cntRef();
				String remarks = annotation.remarks();
				boolean alignRight = annotation.alignRight();
				char padChar = annotation.padChar();
				String defaultValue = annotation.value();

				// field type이 record이고 count-ref가 없는 경우 지정된 길이만큼 record size로 설정한다.
				if(field.getType() == List.class) {
					List<?> recordList = (List<?>)field.get(vo);
					if(recordList == null)	recordList = new ArrayList();
					if(logger.isDebugEnabled()) {
						logger.debug("record[{}][{}]length[{}]cntRef[{}]size[{}}", fieldName, remarks, length, cntRef, recordList.size());
					}

					if(!StringUtils.hasText(cntRef) && length > 0) {
						bout.write(WebTServiceUtil.strFormatter(getEncoding(), recordList.size(), length, true, '0'));	offset += length;
					}
					for(Object record : recordList) {
						messageBuild(offset, record.getClass().getDeclaredFields(), record, bout, maskingList);
					}
				}else {

					byte[] formatValue = null;
					Object value = field.get(vo) == null ? defaultValue : field.get(vo);
					// rule 에 따라 password 변경
					if (fieldName.equals("password")) {
						if(logger.isDebugEnabled()) {
							logger.debug("input password[{}][{}]length[{}]value[{}]", fieldName, remarks, length, value);
						}
						formatValue = WebTServiceUtil.strFormatter(
								getEncoding(),
								convertPasswordRule((String)value),
								16,
								false,
								' ');
					} else {
						formatValue = WebTServiceUtil.strFormatter(
								getEncoding(),
								value,
								length,
								alignRight,
								padChar);
					}

					bout.write(formatValue);

					if(logmaskingProperties.getKeyMap().containsKey(fieldName)) {
						maskingList.add(new MaskingInfo(fieldName, offset, length));
					}

					//offset += length;

					// length 가 0 인 경우 실제 data길이 만큼 계산.
					offset += formatValue.length;

					if(logger.isDebugEnabled()) {
						logger.debug("field[{}][{}]length[{}]cntRef[{}]alignRight[{}]padChar[{}]value[{}]", field.getName(), remarks, length, cntRef, alignRight, padChar, field.get(vo) == null ? defaultValue : field.get(vo));
					}
				}
			}
		}catch(IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IllegalArgumentException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IOException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(WebTServiceException e) {
			throw e;
		}

		return offset;
	}

	/**
	 * 카드 비밀번호를 OCB PW rule에 맞춰서 convert 하여 return
	 * @param srcStr
	 * @return convertedStr
	 */
	private String convertPasswordRule(String srcStr) {

		List<String> REAL_PWD = Arrays.asList(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c",
				"d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
				"y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
				"T", "U", "V", "W", "X", "Y", "Z", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", ".",
				"/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "}", "~" });

		List<String> CONV_PWD = Arrays.asList(new String[] { "9", "0", "1", "2", "3", "4", "5", "6", "7", "8", "Z", "a", "b",
				"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
				"x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
				"S", "T", "U", "V", "W", "X", "Y", "~", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",",
				".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "}" });

		String rstPwd = "";

		for (int i = 0; i < srcStr.length(); i++) {
			logger.debug("REAL_PWD : {}", srcStr.substring(i, i + 1));
			logger.debug("CONV_PWD : {}", (String) CONV_PWD.get(REAL_PWD.indexOf(srcStr.substring(i, i + 1))));

			rstPwd += (String) CONV_PWD.get(REAL_PWD.indexOf(srcStr.substring(i, i + 1)));
		}

		return rstPwd;
	}
}
