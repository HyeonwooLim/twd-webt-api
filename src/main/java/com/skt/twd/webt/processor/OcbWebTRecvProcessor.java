package com.skt.twd.webt.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StreamUtils;

import com.skt.twd.webt.header.TDirectRecvHeader;
import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.logevent.LogEventType;
import com.skt.twd.webt.integration.logevent.MaskingInfo;
import com.skt.twd.webt.integration.logevent.notifier.ByteLogEventNotifier;
import com.skt.twd.webt.integration.logevent.notifier.ObjectLogEventNotifier;
import com.skt.twd.webt.integration.processor.DefaultWebTReceiveProcessor;
import com.skt.twd.webt.integration.service.WebTServiceInfo;
import com.skt.twd.webt.integration.service.WebTServiceUtil;

public class OcbWebTRecvProcessor extends DefaultWebTReceiveProcessor{

	private static Logger logger = LoggerFactory.getLogger(OcbWebTRecvProcessor.class);

	@Autowired
	private WebTProperites properties;

	@Autowired
	private WebTLogMaskingProperties logmaskingProperties;

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receive(final WebTServiceInfo serviceInfo, final ByteArrayInputStream bin, final Object vo) throws WebTServiceException {
		List <Field> memberFieldList = new ArrayList<>();

		Field[] fields = vo.getClass().getDeclaredFields();

		int offset = 0;

		// receive vo logging event
		ObjectLogEventNotifier logObjectEvent = null;
		// receive byte logging event
		ByteLogEventNotifier logStreamEvent = null;
		// log masking field list
		ArrayList<MaskingInfo> maskingList = new ArrayList<>();

		try {
			bin.mark(0);
			byte[] recvBytes = StreamUtils.copyToByteArray(bin);
			bin.reset();
			// byte log event 생성
			logStreamEvent = new ByteLogEventNotifier(LogEventType.RECV_COMPLATE, recvBytes, logmaskingProperties.isEnabled());

			// header field 추출 및 field ordering
			Field headerField = WebTServiceUtil.findHeaderField(fields, memberFieldList);
			Field[] memberField = memberFieldList.toArray(new Field[memberFieldList.size()]);

			// header parsing
			offset = messageParse(offset,
					serviceInfo,
					headerField.get(vo).getClass().getDeclaredFields(),
					headerField.get(vo),
					bin,
					maskingList);
			// call receive header handler
			if(getReceiveHeaderHandler() != null) {
				getReceiveHeaderHandler().handleHeader((IHeaderField)headerField.get(vo), bin, serviceInfo, properties);
			}

			TDirectRecvHeader recvHeader = (TDirectRecvHeader)headerField.get(vo);

			// body parsing
			offset = messageParse(offset,
					serviceInfo,
					memberField,
					vo,
					bin,
					maskingList);
			
			// OCB 조회 결과 코드 처리(header 에 있는 정보를 사용)
			String major_result_cd = recvHeader.st_stop + recvHeader.ngms_coclcd;
			String minor_result_cd = recvHeader.reserved;
			for(Field field : memberField) {
				if (field.getName().equals("major_result_cd")) {
					field.set(vo, major_result_cd);
					if(logger.isDebugEnabled()) {
						logger.debug("field[{}]value[{}]", field.getName(), major_result_cd);
					}
				} else if (field.getName().equals("minor_result_cd")) {
					field.set(vo, minor_result_cd);
					if(logger.isDebugEnabled()) {
						logger.debug("field[{}]value[{}]", field.getName(), minor_result_cd);
					}
				}
			}

			// add masking fields list
			logStreamEvent.addMaskingInfo(maskingList);
			logObjectEvent = new ObjectLogEventNotifier(LogEventType.RECV_COMPLATE, vo, logmaskingProperties.isEnabled());

		}catch(IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IllegalArgumentException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IOException e) {
			throw new WebTServiceException("INF0010", e);
		}finally {
			// 전문 송수신 byte stream 로그가 정상 오류가 발생 했을 때에도 저장될 수 있도록 finally에 추가
			Optional.ofNullable(logStreamEvent).ifPresent(e -> applicationEventPublisher.publishEvent(e));
			Optional.ofNullable(logObjectEvent).ifPresent(e -> applicationEventPublisher.publishEvent(e));
		}
	}
}
