package com.skt.twd.webt.integration.logevent;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * json 로그 저장 시 민감 정보에 대한 masking 처리를 위한 json masking 모듈
 *
 * @author ahnhojung
 *
 */
@SuppressWarnings("serial")
public class JsonMaskingModule extends SimpleModule {
	/**
	 * json field masking 처리를 위한 constructor
	 * @param maskingMap masking 대상 field map
	 * @param isMasking masking 처리 여부
	 */
	public JsonMaskingModule(Map<String, String> maskingMap, boolean isMasking) {
		super("jsonMaskingModule");
		if(isMasking) {
			super.addSerializer(new JsonMaskingSerializer(String.class, maskingMap));
		}
	}
}

@SuppressWarnings("serial")
class JsonMaskingSerializer extends StdSerializer<String> {
	private Map<String, String> maskingMap;
	private final byte maskChar = 0x2A;

    public JsonMaskingSerializer(Class<String> t, Map<String, String> maskingMap) {
        super(t);
        this.maskingMap = maskingMap;
    }

    @Override
    public void serialize(String value,
                          JsonGenerator jgen,
                          SerializerProvider sp) throws IOException, JsonGenerationException {

    		if(maskingMap.containsKey(sp.getGenerator().getOutputContext().getCurrentName()) && !StringUtils.isEmpty(value)) {
    			byte[] buf = new byte[value.getBytes().length];
    			Arrays.fill(buf, 0, buf.length, maskChar);
    			jgen.writeString(new String(buf));
    		}else {
    			jgen.writeString(value);
    		}
    }
}
