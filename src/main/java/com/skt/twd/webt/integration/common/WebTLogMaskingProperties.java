package com.skt.twd.webt.integration.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * WebT Log Masking 처리 설정 properties
 * @author ahnhojung
 *
 */

@ConfigurationProperties(prefix = WebTConst.WEBT_LOGMASKING_PREFIX)
public class WebTLogMaskingProperties implements InitializingBean{
	/** Whether to enable log masking */
	private boolean enabled = false;
	/** log masking 대상 key list  */
	private List<String> keylist = new ArrayList<String>();
	private Map<String, String> keyMap;

	/**
	 * Whether to enable log masking
	 * @return is masking enabled(true/false)
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * set masking enable
	 * @param enabled masking 처리 여부
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * masking 대상 field 명 조회
	 * @return 	masking field name list
	 */
	public List<String> getKeylist() {
		return keylist;
	}

	/**
	 * masking 대상 field 명 설정
	 * @param keylist	masking 대상 field 명 list
	 */
	public void setKeylist(List<String> keylist) {
		this.keylist = keylist;
	}

	/**
	 * masking fiela 명 map
	 * @return masking field name map
	 */
	public Map<String, String> getKeyMap() {
		return keyMap;
	}

	/**
	 * masking 대상 field 조회성능을 위해 list를 map 형태로 변환
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		this.keyMap = keylist.stream()
			.collect(Collectors.toMap( i -> i.toString(), i -> i.toString()));
	}
}
