package com.skt.twd.webt.integration.common;

/**
 * WebT Integration 상수값 정의
 * @author ahnhojung
 */

public class WebTConst {
	/** webt property configuration root key */
	public static final String WEBT_PREFIX = "tdirect.webt";
	/** webt log-masking configuration key */
	public static final String WEBT_LOGMASKING_PREFIX = WEBT_PREFIX + ".logmasking";
	/** webt proxy configuration key */
	public static final String WEBT_PROXY_PREFEX = WEBT_PREFIX + ".proxy";
	/** webt proxy client configuration key */
	public static final String WEBT_PROXY_CLIENT_PREFEX = WEBT_PROXY_PREFEX + ".client";
	/** webt proxy rest service configuration key */
	public static final String WEBT_PROXY_REST_SERVICE_PREFEX = WEBT_PROXY_PREFEX + ".rest";
	/** webt simulation configuration key */
	public static final String WEBT_SIMULATE_SERVICE_PREFEX = WEBT_PREFIX + ".simulate";
	/** webt default proxy rest url */
	public static final String WEBT_DEFAULT_PROXY_URL = "/webt/proxy";
	/** webt default channel name */
	public static final String DEFAULT_CHANNLE_NAME = "default";
	/** webt default send processor bean name */
	public static final String DEFAULT_SEND_PROCESSOR_BEAN_NAME = "defaultWebTSendProcessor";
	/** webt default receive processor bean name */
	public static final String DEFAULT_RECEIVE_PROCESSOR_BEAN_NAME = "defaultWebTReceiveProcessor";
}
