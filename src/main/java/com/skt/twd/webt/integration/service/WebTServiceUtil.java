package com.skt.twd.webt.integration.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.processor.annotation.FieldItem;
import com.skt.twd.webt.integration.processor.annotation.HeaderItem;

/**
 * 클래스 reflection, byte stream처리 등 전문 처리를 위한 utility
 * @author ahnhojung
 *
 */
public class WebTServiceUtil {

	/**
	 * reflection 된 class 의 field array 중 header field를 찾고 member field를 list에 저장
	 * @param fields vo field array
	 * @param memberField member field list
	 * @return header field
	 */
	public static Field findHeaderField(Field [] fields, List <Field> memberField) {
		Field headerField = null;

		for(Field field : fields) {
			Annotation[] annotations = field.getDeclaredAnnotations();
			for(Annotation annotation : annotations) {
				if( annotation instanceof HeaderItem) {
					if(headerField == null) {
						headerField = field;
					}
				}else {
					memberField.add(field);
				}
			}
		}
		return headerField;
	}

	/**
	 * {@link FieldItem} 에 정의된 order에 의해 field array sorting
	 * @param fields field array
	 * @return sorted field array
	 */
	public static Field[] sortOrder(Field[] fields) {
		Arrays.sort(fields, new Comparator<Field>() {
			@Override
			public int compare(Field o1, Field o2) {
				FieldItem f1 = o1.getAnnotation(FieldItem.class);
				FieldItem f2 = o2.getAnnotation(FieldItem.class);
				if(f1 != null && f2 != null) {
					return f1.order() - f2.order();
				}else if(f1 != null && f2 == null) {
					return -1;
				}else{
					return 1;
				}
			}
		});

		return fields;
	}

	/**
	 * 인수로 입력된 length 등 값들 이용하여 fixed length value를 생성
	 * @param value	value
	 * @param length string length
	 * @param rightAlign if true then right align, otherwise left
	 * @param padChar padding character
	 * @return fixed length formatted value
	 */
	public static String strFormatter1(Object value, int length, boolean rightAlign, char padChar) {
		String convStr = "";
		if(value instanceof Integer) {
			rightAlign = true;
			convStr = String.valueOf((Integer) value);
		}else {
			convStr = String.valueOf(value);
		}

		if(rightAlign == true) {
			return StringUtils.leftPad(convStr, length, padChar);
		}else {
			return StringUtils.rightPad(convStr, length, padChar);
		}
	}

	public static byte [] strFormatter(String encoding, Object value, int length, boolean alignRight, char fillChar) throws WebTServiceException{
		if(value instanceof Integer) {
			return strFormatter(String.valueOf((Integer) value).getBytes(), length, true, '0');
		}else if(value instanceof String) {
			try {
				return strFormatter(((String) value).getBytes(encoding), length, alignRight, fillChar);
			} catch (UnsupportedEncodingException e) {
				throw new WebTServiceException("INF0010", e);
			}
		}else {
			throw new WebTServiceException("INF0010", "UnSupported data type [" + value.getClass().getTypeName() + "]");
		}
	}

	public static byte [] strFormatter(byte [] value, int length, boolean alignRight, char fillChar){
    		if(value == null)	return new byte[0];

    		byte [] buf = null;
    		int diff;

    		// length 값이 음수의 경우 입력된 value의 길이로 사용
    		if(length < 0) {
    	        buf  = new byte[value.length];
    	        diff =  0;
    		}else {
    	        buf  = new byte[length];
    	        diff =  length - value.length;
    		}

		if(diff >= 0){
			if(alignRight == true){
				System.arraycopy(value, 0, buf, diff, value.length);
				if(diff > 0){
					for(int i = 0; i < diff; i ++)		buf[i] = (byte)fillChar;
				}
			}else{
				System.arraycopy(value, 0, buf, 0, value.length);
				if(diff > 0){
					for(int i = value.length; i <length; i ++)		buf[i] = (byte)fillChar;
				}
			}
		}else{
			if(alignRight == true){
				System.arraycopy(value, 0, buf, 0, buf.length);
			}else{
				System.arraycopy(value, 0-diff, buf, 0, buf.length);
			}
		}
		return buf;
    }

	public static String getFieldInStream(String encoding, ByteArrayInputStream bin, int length, boolean trim) throws IOException {
		String fieldValue = "";

		if(length > 0) {
			byte [] buf = new byte[length];
			int read = bin.read(buf);
			if(read != length)	throw new IOException("eof");
			return trim == true ? new String(buf, encoding).trim() : new String(buf, encoding);
		}else {
			return fieldValue;
		}
	}
}
