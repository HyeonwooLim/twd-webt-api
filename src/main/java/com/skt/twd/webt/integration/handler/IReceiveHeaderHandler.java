package com.skt.twd.webt.integration.handler;

import java.io.ByteArrayInputStream;

import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.service.WebTServiceInfo;

/**
 * 전문 수신 시 공통부 처리 interface
 *
 * @author ahnhojung
 *
 */
public interface IReceiveHeaderHandler {
	/**
	 * 전문 수신 후 공통부 처리 및 에러 처리를 위한 후처리
	 * @param headerVO	header vo
	 * @param bin	receive stream
	 * @param info	webt service info {@link WebTServiceInfo}
	 * @param properties		webt properties {@link WebTProperites}
	 * @throws WebTServiceException service exception
	 */
	void handleHeader(IHeaderField headerVO, ByteArrayInputStream bin, WebTServiceInfo info, WebTProperites properties) throws WebTServiceException;
}
