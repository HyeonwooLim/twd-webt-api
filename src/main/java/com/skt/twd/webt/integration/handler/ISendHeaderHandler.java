package com.skt.twd.webt.integration.handler;

import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.service.WebTServiceInfo;

/**
 * 전문 송신 시 공통부 처리 interface
 *
 * @author ahnhojung
 *
 */
public interface ISendHeaderHandler {
	/**
	 * 전문 송신 전  공통부 처리를 위한 선 처리
	 * @param headerVO	header vo
	 * @param info	webt service info {@link WebTServiceInfo}
	 * @param properties		webt service properties {@link WebTProperites}
	 * @throws WebTServiceException	service exception
	 */
	void handleHeader(IHeaderField headerVO, WebTServiceInfo info, WebTProperites properties) throws WebTServiceException;
}
