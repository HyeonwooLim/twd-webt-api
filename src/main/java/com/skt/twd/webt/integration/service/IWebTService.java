package com.skt.twd.webt.integration.service;

import com.skt.twd.webt.integration.exception.WebTServiceException;

/**
 * 연동 처리 모듈에 대한 interface
 *
 * @author ahnhojung
 *
 */
public interface IWebTService{
	/**
	 * webt tp service call
	 * @param serviceInfo service info {@link WebTServiceInfo}
	 * @param sendVO sendVO object
	 * @param recvVO recvVO class
	 * @return receive vo object
	 * @throws WebTServiceException webt service exception
	 */
	<T> T service(WebTServiceInfo serviceInfo, Object sendVO, Class<T> recvVO) throws WebTServiceException;
}
