package com.skt.twd.webt.integration.common;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.skt.twd.webt.integration.processor.IWebTReceiveProcessor;
import com.skt.twd.webt.integration.processor.IWebTSendProcessor;

/**
 * WebT 연동 설정 환경 properties
 * @author ahnhojung
 *
 */

@ConfigurationProperties(prefix = WebTConst.WEBT_PREFIX)
public class WebTProperites {
	/** channel 별 webt 설정 정보 */
	private Map<String, WebTChannelProperty> channel;
	/** 송신 character encoding */
	private String sendEncoding = "euc-kr";
	/** 수신 character encoding */
	private String recvEncoding = "euc-kr";
	/** WebT 최대 송신 Buffer 사이즈 */
	private int maxInBufferLength = 32000;
	/** WebT 최대 수신 Buffer 사이즈 */
	private int maxOutBuferLength = 32000;

	/**
	 * webt channel 별 설정 정보
	 * @return  channel별 property
	 */
	public WebTChannelProperty getChannelPropety(String channelName) {
		return channel.get(channelName);
	}

	public Map<String, WebTChannelProperty> getChannel(){
		return channel;
	}

	/**
	 * webt channel 별 설정 정보
	 * @param channel channel별 property
	 */
	public void setChannel(Map<String, WebTChannelProperty> channel) {
		this.channel = channel;
	}

	/**
	 * 송신 encoding set
	 * @return send encoding charset
	 */
	public String getSendEncoding() {
		return sendEncoding;
	}

	/**
	 * 송신 encoding set 설정
	 * @param sendEncoding	send encoding charset
	 */
	public void setSendEncoding(String sendEncoding) {
		this.sendEncoding = sendEncoding;
	}

	/**
	 * 수신 encoding charset
	 * @return receive encoding charset
	 */
	public String getRecvEncoding() {
		return recvEncoding;
	}

	/**
	 * 수신 encoding charset 설정
	 * @param recvEncoding	receive encoding charset
	 */
	public void setRecvEncoding(String recvEncoding) {
		this.recvEncoding = recvEncoding;
	}

	/**
	 * webt max in-buffer size
	 * @return	in-buffer size
	 */
	public int getMaxInBufferLength() {
		return maxInBufferLength;
	}

	/**
	 * set webt max in-buffer size
	 * @param maxInBufferLength	in-buffer size
	 */
	public void setMaxInBufferLength(int maxInBufferLength) {
		this.maxInBufferLength = maxInBufferLength;
	}

	/**
	 * webt max out-buffer size
	 * @return out-buffer size
	 */
	public int getMaxOutBuferLength() {
		return maxOutBuferLength;
	}

	/**
	 * set webt max out-buffer size
	 * @param maxOutBuferLength	out-buffer size
	 */
	public void setMaxOutBuferLength(int maxOutBuferLength) {
		this.maxOutBuferLength = maxOutBuferLength;
	}

	public static class WebTChannelProperty{
		/** WebT send processor bean name. default bean name : defaultSendProcessor */
		private String sendProcessorBeanName = WebTConst.DEFAULT_SEND_PROCESSOR_BEAN_NAME;
		/** WebT receive processor bean name. default bean name : defaultReceiveProcessor */
		private String receiveProcessorBeanName = WebTConst.DEFAULT_RECEIVE_PROCESSOR_BEAN_NAME;
		/** WebT connection pool group name */
		private String poolGroupName;
		/** WebT channel ID */
		private String channelId;
		/** WebT Default Service ID */
		private String serviceId;
		/** WebT 공통부 총송신전문 길이 위치 */
		private int totalMsgLengthPos = 0;
		/** WebT 공통부 총송신전문 길이 */
		private int totalMsgLength = 8;

		/**
		 * webt send processor bean name
		 * @return sendProcessor bean name {@link IWebTSendProcessor}
		 */
		public String getSendProcessorBeanName() {
			return sendProcessorBeanName;
		}

		/**
		 * set webt send processor bean name
		 * @param sendProcessorBeanName sendProcessor bean name {@link IWebTSendProcessor}
		 */
		public void setSendProcessorBeanName(String sendProcessorBeanName) {
			this.sendProcessorBeanName = sendProcessorBeanName;
		}

		/**
		 * webt receive processor bean name
		 * @return receive processor bean name {@link IWebTReceiveProcessor}
		 */
		public String getReceiveProcessorBeanName() {
			return receiveProcessorBeanName;
		}

		/**
		 * set receive processor bean name
		 * @param receiveProcessorBeanName receive processor bean name {@link IWebTReceiveProcessor}
		 */
		public void setReceiveProcessorBeanName(String receiveProcessorBeanName) {
			this.receiveProcessorBeanName = receiveProcessorBeanName;
		}

		/**
		 * webt pool group name
		 * @return	webt pool group name
		 */
		public String getPoolGroupName() {
			return poolGroupName;
		}

		/**
		 * set webt pool group name
		 * @param poolGroupName	webt pool group name
		 */
		public void setPoolGroupName(String poolGroupName) {
			this.poolGroupName = poolGroupName;
		}

		/**
		 * default webt channel id
		 * @return 	channel id
		 */
		public String getChannelId() {
			return channelId;
		}

		/**
		 * set webt channel id
		 * @param channelId	channel id
		 */
		public void setChannelId(String channelId) {
			this.channelId = channelId;
		}

		/**
		 * default webt service id
		 * @return	service id
		 */
		public String getServiceId() {
			return serviceId;
		}

		/**
		 * set webt service id
		 * @param serviceId	service id
		 */
		public void setServiceId(String serviceId) {
			this.serviceId = serviceId;
		}

		/**
		 * webt 전문 송신시 총 길이값 정의 위치
		 * @return
		 */
		public int getTotalMsgLengthPos() {
			return totalMsgLengthPos;
		}

		/**
		 * webt 전문 송신시 총 길이값 정의 위치 설정
		 * @param totalMsgLengthPos
		 */
		public void setTotalMsgLengthPos(int totalMsgLengthPos) {
			this.totalMsgLengthPos = totalMsgLengthPos;
		}

		/**
		 * webt 전문 송신시 총 길이값 정의 길이
		 * @return	총 전문길이값 정의
		 */
		public int getTotalMsgLength() {
			return totalMsgLength;
		}

		/**
		 * webt 전문 송신시 총 길이값 정의 길이 설정
		 * @param totalMsgLength	총 전문길이값
		 */
		public void setTotalMsgLength(int totalMsgLength) {
			this.totalMsgLength = totalMsgLength;
		}

		@Override
		public String toString() {
			return "WebTChannelProperty [sendProcessorBeanName=" + sendProcessorBeanName + ", receiveProcessorBeanName="
					+ receiveProcessorBeanName + ", poolGroupName=" + poolGroupName + ", channelId=" + channelId
					+ ", serviceId=" + serviceId + ", totalMsgLengthPos=" + totalMsgLengthPos + ", totalMsgLength="
					+ totalMsgLength + "]";
		}
	}

	@Override
	public String toString() {
		return "WebTProperites [channel=" + channel + ", sendEncoding=" + sendEncoding + ", recvEncoding="
				+ recvEncoding + ", maxInBufferLength=" + maxInBufferLength + ", maxOutBuferLength=" + maxOutBuferLength
				+ "]";
	}
}
