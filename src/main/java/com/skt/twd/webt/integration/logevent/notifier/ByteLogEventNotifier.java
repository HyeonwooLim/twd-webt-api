package com.skt.twd.webt.integration.logevent.notifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.skt.twd.webt.integration.logevent.AbstractLogEvent;
import com.skt.twd.webt.integration.logevent.LogEventType;
import com.skt.twd.webt.integration.logevent.MaskingInfo;

/**
 * 고정 길이 bytes 로그 event 생성을 위한 notifier
 *
 * @author ahnhojung
 *
 */
@SuppressWarnings("serial")
public class ByteLogEventNotifier extends AbstractLogEvent <byte[]>{

	private List <MaskingInfo> maskingList = new ArrayList<MaskingInfo>();

	/**
	 * byte[] log event notifier constructor
	 * @param type	send/receive {@link LogEventType}
	 * @param source byte[] log source
	 * @param isMasking	masking 여부
	 */
	public ByteLogEventNotifier(LogEventType type, byte[] source, boolean isMasking) {
		super(type, source, isMasking);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public byte[] getEventSource() {
		byte [] buf = (byte[])getSource();
		if(isMasking()) {
			for(MaskingInfo info : getMaskingList()) {
				Arrays.fill(buf, info.getPos(), info.getPos()+info.getLength(), getMaskChar());
			}
		}
		return buf;
	}

	/**
	 * masking 정보 추가
	 * @param maskingList	{@link MaskingInfo} list
	 */
	public void addMaskingInfo(ArrayList<MaskingInfo> maskingList) {
		this.maskingList.addAll(maskingList);
	}

	/**
	 * masking 정보
	 * @return {@link MaskingInfo} list
	 */
	public List<MaskingInfo> getMaskingList() {
		return maskingList;
	}
}
