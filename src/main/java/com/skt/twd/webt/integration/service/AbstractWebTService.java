package com.skt.twd.webt.integration.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.skt.twd.webt.integration.common.WebTBanner;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.common.WebTSimulateProperites;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.processor.IWebTReceiveProcessor;
import com.skt.twd.webt.integration.processor.IWebTSendProcessor;

/**
 * WebT연동 공통처리 추상 클래스
 *
 * @author ahnhojung
 *
 */
public abstract class AbstractWebTService implements IWebTService, InitializingBean{
	private static Logger logger = LoggerFactory.getLogger(AbstractWebTService.class);

	@Autowired
	private WebTProperites properties;
	@Autowired
	private WebTSimulateProperites simulate;
	@Autowired
	private ApplicationContext applicationContext;

	private ConcurrentHashMap<String, IWebTSendProcessor> sendProcessors = new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, IWebTReceiveProcessor> receiveProcessors = new ConcurrentHashMap<>();

	@Override
	public void afterPropertiesSet() throws Exception {

		properties.getChannel()
			.forEach((k, v) -> {
				try {
					sendProcessors.put(k,
							(IWebTSendProcessor)applicationContext.getBean(v.getSendProcessorBeanName()));
				}catch(BeansException e) {
					throw new WebTServiceException("INF0010", e, "[" + k + "] webt channel can not create send processor");
				}
			});
		properties.getChannel()
			.forEach((k, v) -> {
				try {
					receiveProcessors.put(k,
							(IWebTReceiveProcessor)applicationContext.getBean(v.getReceiveProcessorBeanName()));
				}catch(BeansException e) {
					throw new WebTServiceException("INF0010", e, "[" + k + "] webt channel can not create send processor");
				}
			});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T service(WebTServiceInfo serviceInfo, Object sendVO, Class<T> recvVO) throws WebTServiceException{
		ByteArrayInputStream bin = null;
		ByteArrayOutputStream bout = null;

		if(sendVO == null) { throw new WebTServiceException("INF0010", "[" + serviceInfo.getTxCode() + "] sendVO is null"); }
		if(recvVO == null) { throw new WebTServiceException("INF0010", "[" + serviceInfo.getTxCode() + "] recvVO is null"); }

		IWebTSendProcessor sendProcessor = Optional.ofNullable(sendProcessors.get(serviceInfo.getChannelName()))
				.orElseThrow(
						() -> new WebTServiceException("INF0010",
								"[" + serviceInfo.getChannelName() + "] channel send processor not defined."));
		IWebTReceiveProcessor receiveProcessor = Optional.ofNullable(receiveProcessors.get(serviceInfo.getChannelName()))
				.orElseThrow(
						() -> new WebTServiceException("INF0010",
								"[" + serviceInfo.getChannelName() + "] channel receive processor not defined."));

		sendProcessor.setEncoding(properties.getSendEncoding());
		receiveProcessor.setEncoding(properties.getRecvEncoding());

		bout = sendProcessor.send(serviceInfo, sendVO);

		T newInstance = null;
		try {
			newInstance = recvVO.newInstance();
		} catch (InstantiationException e) {
			throw new WebTServiceException("INF0010", e);
		} catch (IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}

		// 대응답 처리 유무
		if(simulate.isEnabled()) {
			bin = findTransaction(serviceInfo);
			if(bin == null) {
				bin = invokeService(serviceInfo, bout);
				// inputstream을 재사용하기 위해 marking
				bin.mark(0);
				receiveProcessor.receive(serviceInfo, bin, newInstance);
				// 정상 거래만 저장
				saveTransaction(serviceInfo, bin);
			}else {
				// 부하테스트를 위한 sleep
				int sleepTime = simulate.getSleepTime();
				if(sleepTime > 0) {
					logger.info(WebTBanner.getAnsiColor("[WebTSimulator] transaction simulation sleep [{}]ms.", WebTBanner.AnsiColor.ANSI_BRIGHT_RED), sleepTime);
					try { Thread.sleep(simulate.getSleepTime()); } catch (Exception ignore) {}
				}

				receiveProcessor.receive(serviceInfo, bin, newInstance);
			}
		}else {
			bin = invokeService(serviceInfo, bout);
			receiveProcessor.receive(serviceInfo, bin, newInstance);
		}

		return newInstance;
	}

	/**
	 * 대응답 처리를 위해 저장된 거래 응답전문 조회
	 * @param serviceInfo service info
	 * @return simulate receive byte
	 */
	private ByteArrayInputStream findTransaction(WebTServiceInfo serviceInfo) {
		ByteArrayInputStream bin = null;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		Path path = Paths.get(simulate.getFilePath(), serviceInfo.getTxCode());

		if(!simulate.containsKey(serviceInfo.getTxCode()))	return null;

		if(Files.exists(path, new LinkOption[]{ LinkOption.NOFOLLOW_LINKS})
				&& !Files.isDirectory(path, new LinkOption[]{ LinkOption.NOFOLLOW_LINKS})) {
			try {
				Files.copy(path, bout);
			} catch (IOException e) {
				logger.error(e.toString(), e);
			}
			bin = new ByteArrayInputStream(bout.toByteArray());

			if(bin.available() == 0) {
				return null;
			}
		}

		logger.info(WebTBanner.getAnsiColor("[WebTSimulator] simulate transaction. [{}].", WebTBanner.AnsiColor.ANSI_BRIGHT_RED), serviceInfo.getTxCode());
		return bin;
	}

	/**
	 * 대응답 처리를 위한 거래 수신 전문 저장
	 * @param serviceInfo service info
	 * @param bin receive byte stream
	 */
	private void saveTransaction(WebTServiceInfo serviceInfo, ByteArrayInputStream bin) {
		Path path = Paths.get(simulate.getFilePath(), serviceInfo.getTxCode());

		try {
			bin.reset();
			byte [] buf = new byte[bin.available()];
			bin.read(buf);
			Path parent = path.getParent();
			if(parent != null) {
				if(!Files.isDirectory(parent)) {
					Files.createDirectory(parent);
				}
				Files.write(path, buf, StandardOpenOption.CREATE);
				logger.info(WebTBanner.getAnsiColor("[WebTSimulator] save transaction. [{}] path[{}]", WebTBanner.AnsiColor.ANSI_BRIGHT_RED), serviceInfo.getTxCode(), path.toAbsolutePath());
			}

		}catch (IOException e) {
			logger.error(e.toString(), e);
		}
	}

	/**
	 * service invoke
	 * @param serviceInfo service info
	 * @param bout send byte stream
	 * @return receive byte stream
	 * @throws WebTServiceException service invoke exception
	 */
	protected abstract ByteArrayInputStream invokeService(WebTServiceInfo serviceInfo, ByteArrayOutputStream bout) throws WebTServiceException;
}
