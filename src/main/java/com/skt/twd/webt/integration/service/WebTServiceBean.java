package com.skt.twd.webt.integration.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.common.WebTProperites.WebTChannelProperty;
import com.skt.twd.webt.integration.exception.WebTException;
import com.skt.twd.webt.integration.exception.WebTServiceException;

import tmax.webt.WebtBuffer;
import tmax.webt.WebtConnection;
import tmax.webt.WebtConnectionPool;
import tmax.webt.WebtException;
import tmax.webt.WebtIOException;
import tmax.webt.WebtRemoteService;
import tmax.webt.WebtServiceFailException;
import tmax.webt.WebtSystem;

/**
 * WebT 라이브러리를 이용한 거래 처리 서비스 클래스
 *
 * @author ahnhojung
 *
 */
public class WebTServiceBean extends AbstractWebTService{

	private static Logger logger = LoggerFactory.getLogger(WebTServiceBean.class);

	@Autowired
	private WebTProperites properties;

	static {
		WebtSystem.setDefaultCharset("euc-kr");
	}

	@Autowired
	public WebTServiceBean() {
		logger.info("[WebTService] WebT service mode start...");
	}

	/**
	 * tp servcie call
	 * @param groupName webt group name
	 * @param serviceId webt service id
	 * @param bout send byte stream
	 * @return receive byte stream
	 * @throws WebtServiceFailException webt service exception
	 * @throws WebtIOException webt i/o exception
	 */
	@SuppressWarnings("deprecation")
	public ByteArrayInputStream tpcall(String groupName, String serviceId, ByteArrayOutputStream bout) throws WebtServiceFailException, WebtIOException{
        WebtConnection connection = null;
        WebtRemoteService service = null;
        WebtBuffer sndBuf = null;
        WebtBuffer rcvBuf = null;
        ByteArrayInputStream bin = null;

        try {
    		if(logger.isDebugEnabled()) {
    			logger.debug("[SEND][{}][{}][{}]", groupName, serviceId, new String(bout.toByteArray(), WebtSystem.getDefaultCharset()));
    		}
	        connection = WebtConnectionPool.getConnection(groupName);
	        service = new WebtRemoteService(serviceId, connection);
	        sndBuf = service.createCarrayBuffer(properties.getMaxInBufferLength());

	        sndBuf.setBytes(bout.toByteArray());

	        rcvBuf = service.tpcall(sndBuf);
	        byte[] recvBytes = rcvBuf.getBytes();
	        if(logger.isDebugEnabled()) {
	        	logger.debug("[RECV][{}][{}][{}]", groupName, serviceId, new String(recvBytes, WebtSystem.getDefaultCharset()));
	        }
	        bin = new ByteArrayInputStream(recvBytes);
        } catch (UnsupportedEncodingException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("unsupported encoding excpetion to print exception log message.[{}]", e.getMessage());
			}
		} finally {
			if (connection != null)
				WebtConnectionPool.putConnection(connection);
//			if (connection != null && connection.isValidHandle()) {
//				connection.close();
//			}
        }
        return bin;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ByteArrayInputStream invokeService(WebTServiceInfo serviceInfo, ByteArrayOutputStream bout)
			throws WebTServiceException {
		try {
	        WebTChannelProperty channelPropety = properties.getChannelPropety(serviceInfo.getChannelName());
	        if(channelPropety == null) {
	        	throw new  WebTServiceException("INF0010", "[" + serviceInfo.getChannelName() + "] channel name not exist");
	        }

	        String groupName = StringUtils.defaultIfBlank(serviceInfo.getPoolGroupName(), channelPropety.getPoolGroupName());
	        String serviceId = StringUtils.defaultIfBlank(serviceInfo.getServiceId(), channelPropety.getServiceId());

			return tpcall(groupName, serviceId, bout);
		} catch (WebtIOException e) {
			throw new WebTException(e);
		} catch (WebtException e) {
			throw new WebTException(e);
		}
	}
}
