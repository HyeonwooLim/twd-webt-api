package com.skt.twd.webt.integration.handler;

import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.service.WebTServiceInfo;
import com.skt.twd.webt.integration.vo.header.DefaultSendHeader;

/**
 * 전문 송신시 공통처리 ( 송신시 기본값 설정 등 )에 대한 기본제공 클래스
 *
 * @author ahnhojung
 *
 */
public class DefaultSendHeaderHandler implements ISendHeaderHandler{

    /**
     * {@inheritDoc}
     */
	@Override
	public void handleHeader(IHeaderField header, WebTServiceInfo info, WebTProperites properties) {

		DefaultSendHeader headerVO = (DefaultSendHeader)header;

		headerVO.tx_code = info.getTxCode();
		headerVO.channel_id = info.getChannelId() != null
					? info.getChannelId()
					: properties.getChannelPropety(info.getChannelName()).getChannelId();

	}
}
