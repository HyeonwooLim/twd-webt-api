package com.skt.twd.webt.integration.logevent.notifier;

import com.skt.twd.webt.integration.logevent.AbstractLogEvent;
import com.skt.twd.webt.integration.logevent.LogEventType;

/**
 * VO 로그 event 생성을 위한 notifier
 *
 * @author ahnhojung
 *
 */
@SuppressWarnings("serial")
public class ObjectLogEventNotifier extends AbstractLogEvent <Object>{

	/**
	 * VO log event notifier constructor
	 * @param type	send/receive {@link LogEventType}
	 * @param source vo log source
	 * @param isMasking	masking 여부
	 */
	public ObjectLogEventNotifier(LogEventType type, Object source, boolean isMasking) {
		super(type, source, isMasking);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getEventSource() {
		return (Object)getSource();
	}
}
