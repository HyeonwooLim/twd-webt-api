package com.skt.twd.webt.integration.logevent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.logevent.notifier.ByteLogEventNotifier;
import com.skt.twd.webt.integration.logevent.notifier.ObjectLogEventNotifier;

/**
 * 로그 event 발생 시 로그 유형에 따라 로그 처리를 위한 기본으로 설정된 log event listener
 * webt 연동 시 생성되는 로그는
 * <ul>
 * <li> webt 송/수신 고정길이 전문 byte[] log</li>
 * <li> 전문 송/수신 시 처리된 VO로그</li>
 * </ul>
 * 두가지의 event가 발생 되며 로그 저장을 위해 업무에 맞게 로그를 저장 하도록 수정하여 사용한다.
 * <p>
 * 로그 event 수신 방법은 {@link org.springframework.context.event.EventListener} annotation의 condition 정보를 이용하여
 * <p>송신/수신 유형으로 구분하며 두가지 로그 event는 argument type에 의해 정의 된다.
 *
 * <ul><li> webt byte[] 로그 처리 방법</li>
 * <pre>
 * &#64;EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).SEND_COMPLATE")
 * public void someSendByteLog(ByteLogEventNotifier logEvent) { // 송신 로그 event
 *     byte[] sendBytes = logEvent.getEventSource();
 *     ...
 *
 * &#64;EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).RECV_COMPLATE")
 * public void someRecvByteLog(ByteLogEventNotifier logEvent) { // 수신 로그 event
 *     byte[] recvBytes = logEvent.getEventSource();
 *     ...
 * </pre>
 * </ul>
 * <ul>
 * <li> VO 객체 로그 처리 방법 </li>
 * <pre>
 * &#64;EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).SEND_COMPLATE")
 * public void someSendVOLog(ObjectLogEventNotifier logEvent) { // 송신 로그 event
 *     Object sendVO = logEvent.getEventSource();
 *     ...
 *
 * &#64;EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).RECV_COMPLATE")
 * public void someRecvVOLog(ObjectLogEventNotifier logEvent) { // 수신 로그 event
 *     Object recvVO = logEvent.getEventSource();
 *     ...
 * </pre>
 * </ul>
 *
 *
 * @author ahnhojung
 *
 */
@Component
public class DefaultLogEventListener{
	/** webt 송/수신 bytes 고정길이 logger */
	private static Logger traceLogger = LoggerFactory.getLogger("TRANS_LOG");
	/** 송/수신 VO logger */
	private static Logger messageLogger = LoggerFactory.getLogger("MESSAGE_LOG");

	@Autowired
	private WebTLogMaskingProperties  properties;

	private ObjectMapper mapper = new ObjectMapper();
	/**
	 * 송신 webt bytes log 처리
	 * @param logEvent bytes log event
	 */
	@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).SEND_COMPLATE")
	public void sendByteComplate(ByteLogEventNotifier logEvent) {
		if(traceLogger.isTraceEnabled()) {
			traceLogger.trace("SEND: [{}]", new String(logEvent.getEventSource()));
		}
	}

	/**
	 * 수신 webt bytes log 처리
	 * @param logEvent bytes log event
	 */
	@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).RECV_COMPLATE")
	public void recvByteComplate(ByteLogEventNotifier logEvent) {
		if(traceLogger.isTraceEnabled()) {
			traceLogger.trace("RECV: [{}]", new String(logEvent.getEventSource()));
		}
	}

	/**
	 * 송신 VO log 처리
	 * @param logEvent vo log event
	 */
	@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).SEND_COMPLATE")
	public void sendObjectComplate(ObjectLogEventNotifier logEvent) {
		if(messageLogger.isTraceEnabled()) {
			// json annotation ignore
			mapper.disable(MapperFeature.USE_ANNOTATIONS);
			mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
			// json log 출력시 지정된 field에 대해서 log masking
			mapper.registerModule(new JsonMaskingModule(properties.getKeyMap(), logEvent.isMasking()));

			try {
				messageLogger.trace("SEND: {}" , mapper.writerWithDefaultPrettyPrinter().writeValueAsString(logEvent.getEventSource()));
				// messageLogger.trace("SEND: {}" , mapper.writeValueAsString(logEvent.getEventSource()));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 수신 VO log 처리
	 * @param logEvent vo log event
	 */
	@EventListener(condition="#logEvent.type==T(com.skt.twd.integration.webt.logevent.LogEventType).RECV_COMPLATE")
	public void recvObjectComplate(ObjectLogEventNotifier logEvent) {
		if(messageLogger.isTraceEnabled()) {
			// json annotation ignore
			mapper.disable(MapperFeature.USE_ANNOTATIONS);
			mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
			// json log 출력시 지정된 field에 대해서 log masking
			mapper.registerModule(new JsonMaskingModule(properties.getKeyMap(), logEvent.isMasking()));
			try {
				messageLogger.trace("RECV: {}" , mapper.writerWithDefaultPrettyPrinter().writeValueAsString(logEvent.getEventSource()));
				// messageLogger.trace("RECV: {}" , mapper.writeValueAsString(logEvent.getEventSource()));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
	}
}
