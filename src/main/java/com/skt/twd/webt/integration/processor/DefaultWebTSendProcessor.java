package com.skt.twd.webt.integration.processor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StringUtils;

import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.common.WebTProperites.WebTChannelProperty;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.handler.ISendHeaderHandler;
import com.skt.twd.webt.integration.logevent.LogEventType;
import com.skt.twd.webt.integration.logevent.MaskingInfo;
import com.skt.twd.webt.integration.logevent.notifier.ByteLogEventNotifier;
import com.skt.twd.webt.integration.logevent.notifier.ObjectLogEventNotifier;
import com.skt.twd.webt.integration.processor.annotation.FieldItem;
import com.skt.twd.webt.integration.service.WebTServiceInfo;
import com.skt.twd.webt.integration.service.WebTServiceUtil;

/**
 * WebT 전문 송신 처리 구현체
 *
 * @author ahnhojung
 *
 */
public class DefaultWebTSendProcessor implements IWebTSendProcessor{
	private static Logger logger = LoggerFactory.getLogger(DefaultWebTSendProcessor.class);

	private ISendHeaderHandler headerHandler;

	@Autowired
	private WebTProperites properties;
	@Autowired
	private WebTLogMaskingProperties logmaskingProperties;

	// default send encoding
	private String encoding = "euc-kr";

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ByteArrayOutputStream send(WebTServiceInfo serviceInfo, Object vo) throws WebTServiceException {
		// byte stream send buffer
		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		List <Field> memberFieldList = new ArrayList<Field>();

		Field[] fields = vo.getClass().getDeclaredFields();

		int offset = 0;

		// logging event
		ObjectLogEventNotifier logObjectEvent = null;
		ByteLogEventNotifier logStreamEvent = null;

		// log masking field list
		ArrayList<MaskingInfo> maskingList = new ArrayList<MaskingInfo>();

		try {
			// header field 추출 및 field ordering
			Field headerField = WebTServiceUtil.findHeaderField(fields, memberFieldList);

			// call send header handler
			if(headerHandler != null) {
				headerHandler.handleHeader((IHeaderField)headerField.get(vo), serviceInfo, properties);
			}

			logObjectEvent = new ObjectLogEventNotifier(LogEventType.SEND_COMPLATE, vo, logmaskingProperties.isEnabled());
			// vo log event publish
			applicationEventPublisher.publishEvent(logObjectEvent);

			Field[] memberField = memberFieldList.toArray(new Field[memberFieldList.size()]);

			// byte buffer for object parsing
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			// header stream build
			offset = messageBuild(offset,
						headerField.get(vo).getClass().getDeclaredFields(),
						headerField.get(vo),
						buffer,
						maskingList);

			// body stream build
			offset = messageBuild(offset,
						memberField,
						vo,
						buffer,
						maskingList);

			// channel 별 webt 설정 정보
			WebTChannelProperty channelPropety = properties.getChannelPropety(serviceInfo.getChannelName());
			int totalMsgPos = 0;
			int totalMsgLength = 8;

			// 호출 시 channel정보에 대한 validation이 선 수행
			// 등록된 channel정보 존재여부 확인
			if(channelPropety != null){
				totalMsgPos = channelPropety.getTotalMsgLengthPos();
				totalMsgLength = channelPropety.getTotalMsgLength();
			}

			// total header length setting
			byte[] sendBuffer = buffer.toByteArray();
			System.arraycopy(
					WebTServiceUtil.strFormatter(
							encoding,
							offset,
							totalMsgLength,
							true,
							'0'),
					0, sendBuffer, totalMsgPos, totalMsgLength);

			// header & body write
			bout.write(sendBuffer);

			// send byte log event publish
			logStreamEvent = new ByteLogEventNotifier(LogEventType.SEND_COMPLATE, bout.toByteArray(), logmaskingProperties.isEnabled());
			logStreamEvent.addMaskingInfo(maskingList);

			applicationEventPublisher.publishEvent(logStreamEvent);

			return bout;
		}catch(IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IllegalArgumentException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IOException e) {
			throw new WebTServiceException("INF0010", e);
		}
	}

	/**
	 * field array의 정보를 이용하여 전송 할 byte[] data stream 생성
	 * @param offset messgae build current offset
	 * @param fields send vo field array
	 * @param vo send vo
	 * @param bout send byte streawm
	 * @param maskingList masking field
	 * @return build offset
	 * @throws WebTServiceException message build exception
	 */
	@SuppressWarnings("rawtypes")
	protected int messageBuild(int offset, Field[] fields, Object vo, ByteArrayOutputStream bout,
			ArrayList<MaskingInfo> maskingList) throws WebTServiceException {

		try {
			// annotation 에 정의된 order로 sort
			fields = WebTServiceUtil.sortOrder(fields);
			// 송신 전문에 record의 count-reference field가 있는 경우 값을 미리 설정
			setRecordFieldCount(fields, vo);

			for(Field field : fields) {
				field.setAccessible(true);
				FieldItem annotation = field.getDeclaredAnnotationsByType(FieldItem.class)[0];

				String fieldName = field.getName();
				int length = annotation.length();
				String cntRef = annotation.cntRef();
				String remarks = annotation.remarks();
				boolean alignRight = annotation.alignRight();
				char padChar = annotation.padChar();
				String defaultValue = annotation.value();

				// field type이 record이고 count-ref가 없는 경우 지정된 길이만큼 record size로 설정한다.
				if(field.getType() == List.class) {
					List<?> recordList = (List<?>)field.get(vo);
					if(recordList == null)	recordList = new ArrayList();
					if(logger.isDebugEnabled()) {
						logger.debug("record[{}][{}]length[{}]cntRef[{}]size[{}}", field.getName(), remarks, length, cntRef, recordList.size());
					}

					if(!StringUtils.hasText(cntRef) && length > 0) {
						bout.write(WebTServiceUtil.strFormatter(encoding, recordList.size(), length, true, '0'));	offset += length;
					}
					for(Object record : recordList) {
						messageBuild(offset, record.getClass().getDeclaredFields(), record, bout, maskingList);
					}
				}else {
					byte[] formatValue = WebTServiceUtil.strFormatter(
							encoding,
							field.get(vo) == null ? defaultValue : field.get(vo),
							length,
							alignRight,
							padChar);

					bout.write(formatValue);

					if(logmaskingProperties.getKeyMap().containsKey(fieldName)) {
						maskingList.add(new MaskingInfo(fieldName, offset, length));
					}

					//offset += length;

					// length 가 0 인 경우 실제 data길이 만큼 계산.
					offset += formatValue.length;

					if(logger.isDebugEnabled()) {
						logger.debug("field[{}][{}]length[{}]cntRef[{}]alignRight[{}]padChar[{}]value[{}]", field.getName(), remarks, length, cntRef, alignRight, padChar, field.get(vo) == null ? defaultValue : field.get(vo));
					}
				}
			}
		}catch(IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IllegalArgumentException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IOException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(WebTServiceException e) {
			throw e;
		}

		return offset;
	}

	/**
	 * 메시지 송신 시 반복부가 있는 경우 반복부 count field가 지정되어 있는지 확인 및 지정된 경우 반복부 record count setting
	 * @param fields send vo field array
	 * @param vo send vo
	 */
	protected void setRecordFieldCount(Field [] fields, Object vo) throws WebTServiceException {
		Map<String, Field> keyMap = new HashMap<String, Field>();

		try {
			for(Field field : fields) {
				field.setAccessible(true);
				if(field.getType() == List.class) {
					List<?> recordList = (List<?>)field.get(vo);

					FieldItem annotation = field.getDeclaredAnnotationsByType(FieldItem.class)[0];
					String cntRef = annotation.cntRef();
					if(StringUtils.hasText(cntRef)) {
						Field cntField = keyMap.get(cntRef.trim());
						if (cntField == null) {
							throw new WebTServiceException("INF0020", "message build error. [refcnt=" + cntRef.trim() + "] in [" + field.getName() + "] field does not exist");
						}

						if(cntField.getType() == Integer.TYPE) {
							cntField.set(vo, recordList==null ? 0 : recordList.size());
						}else {
							cntField.set(vo, recordList==null ? "0" : String.valueOf(recordList.size()));
						}
					}
				}else {
					keyMap.put(field.getName(), field);
				}
			}
		}catch(IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IllegalArgumentException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(WebTServiceException e) {
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSendHeaderHandler(ISendHeaderHandler handler) {
		this.headerHandler = handler;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ISendHeaderHandler getSendHeaderHandler() {
		return this.headerHandler;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEncoding() {
		return this.encoding;
	}
}
