package com.skt.twd.webt.integration.vo.header;

import java.util.List;

import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.processor.annotation.FieldItem;

/**
 * 수신 전문 header ( default )
 * @author ahnhojung
 *
 */
public class DefaultRecvHeader implements IHeaderField{
	/** Stream Length */
	@FieldItem(order=0, length=8, remarks="StreamLength")
	public String totalLength;
	/** GlobalID */
	@FieldItem(order=1, length=32, remarks="GlobalID")
	public String global_id;
	/** 거래코드 */
	@FieldItem(order=2, length=24, remarks="거래코드")
	public String tx_code;
	/** 요청응답구분 */
	@FieldItem(order=3, length=1, remarks="요청응답구분")
	public String send_rspn_type;
	/** 처리결과 */
	@FieldItem(order=4, length=1, remarks="처리결과")
	public String err_flag;
	/** 오류코드 */
	@FieldItem(order=5, length=9, remarks="오류코드")
	public String err_code;
	/** 오류메시지 */
	@FieldItem(order=6, length=80, remarks="오류메시지")
	public String err_msg;
	/** 종별코드 */
	@FieldItem(order=7, length=4, remarks="종별코드")
	public String kind_code;
	/** 채널코드 */
	@FieldItem(order=8, length=10, remarks="채널코드")
	public String channel_id;
	/** 주운영시스템상태 */
	@FieldItem(order=9, length=1, remarks="주운영시스템상태")
	public String st_stop;
	/** 회사구분코드 */
	@FieldItem(order=10, length=1, remarks="회사구분코드")
	public String ngms_coclcd;
	/** 임시필러 */
	@FieldItem(order=11, length=18, remarks="임시필러")
	public String reserved;
	/** MSG카운트 */
	@FieldItem(order=12, length=1, remarks="MSG카운트")
	public String msg_cnt;

	@FieldItem(order=13, cntRef="msg_cnt", remarks="ERROR_Record")
	public List <MCG_ERRORHEADER>errorRecord;

	public static class MCG_ERRORHEADER {
		/** 메세지구분 */
		@FieldItem(order=0, length=1, remarks="에러메세지구분")
		public String st_msg;
		/** 에러메세지코드 */
		@FieldItem(order=1, length=9, remarks="에러메세지코드")
		public String st_cd;
		/** 에러메시지 */
		@FieldItem(order=2, length=80, remarks="에러메시지")
		public String error_msg;
		/** 시스템메시지 */
		@FieldItem(order=3, length=50, remarks="시스템메시지")
		public String error_msgSys;
	}
}
