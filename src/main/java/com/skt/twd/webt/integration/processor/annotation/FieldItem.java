package com.skt.twd.webt.integration.processor.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 전문 VO의 전문 field 정의를 위한 annotation
 *
 * @author ahnhojung
 *
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldItem{
	// field order
	int order() default 0;
	// field remarks
	String remarks() default "";
	// field default value
	String value() default "";
	// field length
	int length() default 0;
	// field value align
	boolean alignRight() default false;
	// record size reference field
	String cntRef() default "";
	// value padding char
	char padChar() default ' ';

	/**
	 * 반복되는 리스트의 사이즈가 고정되어 있는 경우 사용.
	 * cntRef가 설정되어 있다면 cntRef가 우선적으로 적용됨.
	 * @addedBy BD 정조영
	 * @date 2018-07-30
	 */
	int fixedCnt() default 0;
}
