package com.skt.twd.webt.integration.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * WebT simulation 처리 환경 properties
 * @author ahnhojung
 *
 */
@ConfigurationProperties(prefix = WebTConst.WEBT_SIMULATE_SERVICE_PREFEX)
public class WebTSimulateProperites implements InitializingBean{
	private static Logger logger = LoggerFactory.getLogger(WebTSimulateProperites.class);
	/** simulation 기능 사용여부 */
	private boolean enabled = false;
	/** 거래 전문 저장 위치. default : ./simulate/" */
	private String filePath = "./simulate/";
	/** 부하테스트를 위한 waiting time 'min-max' millisecond  */
	private String waiting = "0-0";

	private int min = 0;
	private int max = 0;

	private List<String> txcode = new ArrayList<>();
	private Map<String, String> keyMap;

	/**
	 * webt simulator enabled
	 * @return	simulator enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * set webt simulator enabled
	 * @param enabled	webt simulator enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * 대응답 처리를 위한 수신 전문 저장 file path
	 * @return	simulate tx file path
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * 대응답 처리를 위한 수신 전문 저장 file path 설정
	 * @param filePath	simulate tx file path
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * 대응답 처리 대상 거래 txcode list 설정
	 * @param txcode 	txcode list
	 */
	public void setTxcode(List<String> txcode) {
		this.txcode = txcode;
	}

	/**
	 * 대응답 처리 대상 거래 txcode list
	 * @return	txcode list
	 */
	public List<String> getTxcode() {
		return txcode;
	}

	/**
	 * simulate 대상 txcode 존재여부
	 * @param txcode		txcode
	 * @return	txcode 존재여부
	 */
	public boolean containsKey(String txcode) {
		return keyMap.containsKey(txcode);
	}

	/**
	 * 거래 지연 설정을 위한 delay 값 ( min-max millisecond )
	 * @return	delay time
	 */
	public String getWaiting() {
		return waiting;
	}

	/**
	 * 거래 지연 설정을 위한 delay 값 ( min-max millisecond ) 설정
	 * @param waiting	delay time
	 */
	public void setWaiting(String waiting) {
		this.waiting = waiting;
	}

	/**
	 * 거래 지연 설정 값에 따른 random delay time
	 * @return	delay time
	 */
	public int getSleepTime() {
		return (int) (Math.random() * (this.max - min + 1)) + min;
	}

	/**
	 * simulate 대상 거래 존재여부 처리를 위해 txcode list를 map으로 변환
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		this.keyMap = txcode.stream()
			.collect(Collectors.toMap( i -> i.toString(), i -> i.toString()));

		try {
			String[] minmax = waiting.split("-");
			this.min = Integer.parseInt(minmax[0].trim());
			this.max = Integer.parseInt(minmax[1].trim());
		}catch(Exception ignore) {}

		if(isEnabled()) {
			logger.info(WebTBanner.getAnsiColor("[WebTSimulator] webt simulate mode is true.", WebTBanner.AnsiColor.ANSI_BRIGHT_RED));
			logger.info(WebTBanner.getAnsiColor("[WebTSimulator] webt simulated txcode {}", WebTBanner.AnsiColor.ANSI_BRIGHT_RED), getTxcode().toString());
		}
	}
}
