package com.skt.twd.webt.integration.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.skt.twd.webt.integration.common.WebTConst;
import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.common.WebTSimulateProperites;
import com.skt.twd.webt.integration.common.WebTProxyProperties.WebTProxyClientProperties;
import com.skt.twd.webt.integration.handler.DefaultReceiveHeaderHandler;
import com.skt.twd.webt.integration.handler.DefaultSendHeaderHandler;
import com.skt.twd.webt.integration.handler.IReceiveHeaderHandler;
import com.skt.twd.webt.integration.handler.ISendHeaderHandler;
import com.skt.twd.webt.integration.processor.DefaultWebTReceiveProcessor;
import com.skt.twd.webt.integration.processor.DefaultWebTSendProcessor;
import com.skt.twd.webt.integration.processor.IWebTReceiveProcessor;
import com.skt.twd.webt.integration.processor.IWebTSendProcessor;
import com.skt.twd.webt.integration.service.IWebTService;
import com.skt.twd.webt.integration.service.WebTProxyServiceBean;
import com.skt.twd.webt.integration.service.WebTServiceBean;
import com.skt.twd.webt.proxy.WebTProxyController;

/**
 * WebT 모듈사용을 위한 bean configuration define
 *
 * @author ahnhojung
 *
 */
@Configuration
@EnableConfigurationProperties(value = { WebTProperites.class, WebTLogMaskingProperties.class, WebTProxyClientProperties.class, WebTSimulateProperites.class})
public class WebTConfiguration {

	@Bean
	@ConditionalOnProperty(prefix=WebTConst.WEBT_PROXY_CLIENT_PREFEX, name = "enabled", havingValue="false", matchIfMissing = true)
	public IWebTService defaultWebTProcessor() {
		return new WebTServiceBean();
	}
	@Bean
	@ConditionalOnProperty(prefix=WebTConst.WEBT_PROXY_CLIENT_PREFEX, name = "enabled", havingValue="true", matchIfMissing = false)
	public IWebTService webTProxyProcessor() {
		return new WebTProxyServiceBean();
	}

	@Bean
	@ConditionalOnBean(IWebTService.class)
	public IWebTSendProcessor defaultWebTSendProcessor() {
		 DefaultWebTSendProcessor sendProcessor = new DefaultWebTSendProcessor();
		 sendProcessor.setSendHeaderHandler(defaultSendHeaderHandler());
		 return sendProcessor;
	}

	@Bean
	@ConditionalOnBean(IWebTService.class)
	public IWebTReceiveProcessor defaultWebTReceiveProcessor() {
		DefaultWebTReceiveProcessor recvProcessor = new DefaultWebTReceiveProcessor();
		recvProcessor.setReceiveHeaderHandler(defaultReceiveHeaderHandler());
		return recvProcessor;
	}

	@Bean
	@ConditionalOnMissingBean
	public ISendHeaderHandler defaultSendHeaderHandler() {
		return new DefaultSendHeaderHandler();
	}
	@Bean
	@ConditionalOnMissingBean
	public IReceiveHeaderHandler defaultReceiveHeaderHandler() {
		return new DefaultReceiveHeaderHandler();
	}

	@Bean
	@ConditionalOnProperty(prefix=WebTConst.WEBT_PROXY_REST_SERVICE_PREFEX, name = "enabled", havingValue="true")
	public WebTProxyController proxyController() {
		WebTProxyController controller = new WebTProxyController();
		return controller;
	}
}
