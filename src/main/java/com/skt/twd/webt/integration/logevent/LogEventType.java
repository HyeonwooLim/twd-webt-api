package com.skt.twd.webt.integration.logevent;

/**
 * 로그 event type 정의 enum 클래스
 *
 * @author ahnhojung
 *
 */
public enum LogEventType {
	SEND_COMPLATE, RECV_COMPLATE
}
