package com.skt.twd.webt.integration.common;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * WebT Proxy 처리 설정 properties
 * @author ahnhojung
 *
 */
@ConfigurationProperties(prefix = WebTConst.WEBT_PROXY_PREFEX)
public class WebTProxyProperties {
	@ConfigurationProperties(prefix = WebTConst.WEBT_PROXY_CLIENT_PREFEX)
	public static class WebTProxyClientProperties{
		/** Whether to enable proxy mode if the webT proxy service server are available. */
		private boolean enabled = false;
		/** webT service proxy server url */
		private String url;
		/** webT service proxy timeout */
		private int timeout = 10000;

		/**
		 * webt simulator enabled
		 * @return	simulator enabled
		 */
		public boolean isEnabled() {
			return enabled;
		}

		/**
		 * set webt simulator enabled
		 * @param enabled	simulator enabled
		 */
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		/**
		 * proxy service url
		 * @return	url
		 */
		public String getUrl() {
			return url;
		}

		/**
		 * set proxy service url
		 * @param url	url
		 */
		public void setUrl(String url) {
			this.url = url;
		}

		/**
		 * proxy rest timeout
		 * @return	timeout
		 */
		public int getTimeout() {
			return timeout;
		}

		/**
		 * set proxy rest timeout
		 * @param timeout	timeout
		 */
		public void setTimeout(int timeout) {
			this.timeout = timeout;
		}

	}

	@ConfigurationProperties(prefix = WebTConst.WEBT_PROXY_REST_SERVICE_PREFEX)
	public static class WebTRestServiceProperties{
		/** Whether to enable proxy service */
		private boolean enabled = false;

		/**
		 * webt rest service enabled
		 * @return	rest enabled
		 */
		public boolean isEnabled() {
			return enabled;
		}

		/**
		 * set webt rest service enabled
		 * @param enabled rest enabled
		 */
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
	}
}
