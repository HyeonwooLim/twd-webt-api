package com.skt.twd.webt.integration.processor;

import java.io.ByteArrayOutputStream;

import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.handler.ISendHeaderHandler;
import com.skt.twd.webt.integration.service.WebTServiceInfo;

/**
 * 전문 송신 처리 interface
 *
 * @author ahnhojung
 *
 */
public interface IWebTSendProcessor{
	/**
	 * webt 전문 수신 처리
	 * @param serviceInfo service info {@link WebTServiceInfo}
	 * @param vo send vo
	 * @return webt send byte stream
	 * @throws WebTServiceException message build exception
	 */
	ByteArrayOutputStream send(WebTServiceInfo serviceInfo, Object vo) throws WebTServiceException;

	/**
	 * 송신 header handler 설정
	 * @param handler send header handler
	 */
	void setSendHeaderHandler(ISendHeaderHandler handler);

	/**
	 * 송신 header handler
	 * @return send header handler
	 */
	ISendHeaderHandler getSendHeaderHandler();

	/**
	 * 송신 encoding 설정
	 * @param encoding encoding
	 */
	void setEncoding(String encoding);

	/**
	 * 송신 encoding
	 * @return encoding
	 */
	String getEncoding();
}
