package com.skt.twd.webt.integration.exception;

import com.skt.twd.webt.utils.exceptions.InterfaceClientException;

/**
 * WebT 연계 물리적 에러 ( connection refuse, 설정 오류 ) 발생 exception
 *
 * @author ahnhojung
 *
 */
@SuppressWarnings("serial")
public class WebTException extends InterfaceClientException{

	public WebTException(Throwable cause) {
        super("INF0001", InterfaceType.SWING, cause);
    }
}
