package com.skt.twd.webt.integration.handler;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.service.WebTServiceInfo;
import com.skt.twd.webt.integration.vo.header.DefaultRecvHeader;
import com.skt.twd.webt.utils.exceptions.InterfaceClientException;
import com.skt.twd.webt.utils.exceptions.InterfaceClientException.InterfaceType;

/**
 * 전문 수신시 공통처리 ( 오류처리, 수신시 기본값 설정 등 )에 대한 기본제공 클래스
 *
 * @author ahnhojung
 *
 */
public class DefaultReceiveHeaderHandler implements IReceiveHeaderHandler{
	/** WEBT 거래 성공 */
	private final String SUCCESS = "0";
	/** WEBT NGM Error */
	private final String NGM_ERROR = "1";
	/** WEBT MCG Error */
	private final String MCG_ERROR = "2";

    /**
     * {@inheritDoc}
     */
	@Override
	public void handleHeader(IHeaderField headerVO, ByteArrayInputStream bin, WebTServiceInfo info, WebTProperites properties) throws WebTServiceException {

		DefaultRecvHeader header = (DefaultRecvHeader)headerVO;
		if(SUCCESS.equals(header.err_flag)) {
			return;
		}else if(NGM_ERROR.equals(header.err_flag)) {
			List <Map<String, String>> errorStack = new ArrayList<Map<String, String>>();
			for(DefaultRecvHeader.MCG_ERRORHEADER error : header.errorRecord) {
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("st_msg", error.st_msg);
				errorMap.put("st_cd", error.st_cd);
				errorMap.put("error_msg", error.error_msg);
				errorMap.put("error_msgSys", error.error_msgSys);
				errorStack.add(errorMap);
			}
			if(errorStack.isEmpty()) {
				throw new WebTServiceException("INF0020", "WebT common error record size 0. return code [" + header.err_flag + "]");
			}

			InterfaceClientException e = new InterfaceClientException(errorStack.get(0).get("st_cd"), InterfaceType.SWING);
			e.setOrgExceptionCode(errorStack.get(0).get("st_cd"));
			e.setOrgMessage(errorStack.get(0).get("st_msg"));
			e.setDebugMessage("[" + errorStack.get(0).get("st_cd") + "]:" + errorStack.get(0).get("st_msg") + ":" + errorStack.get(0).get("error_msg") + ":" + errorStack.get(0).get("error_msgSys"));
			e.setErrorStacks(errorStack);
			throw e;

		}else if(MCG_ERROR.equals(header.err_flag)) {
			List <Map<String, String>> errorStack = new ArrayList<Map<String, String>>();

			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put("st_msg", header.err_code.trim());
			errorMap.put("st_cd", header.err_msg.trim());
			errorMap.put("error_msg", "");
			errorMap.put("error_msgSys", "");
			errorStack.add(errorMap);

			InterfaceClientException e = new InterfaceClientException(errorStack.get(0).get("st_cd"), InterfaceType.SWING);
			e.setOrgExceptionCode(errorStack.get(0).get("st_cd"));
			e.setOrgMessage(errorStack.get(0).get("st_msg"));
			e.setDebugMessage("[" + errorStack.get(0).get("st_cd") + "]:" + errorStack.get(0).get("st_msg"));
			e.setErrorStacks(errorStack);
			throw e;
		}else {
			throw new WebTServiceException("INF0020", "Unknown ErrorCode [" + header.err_flag + "]");
		}
	}
}
