package com.skt.twd.webt.integration.logevent;

import org.springframework.context.ApplicationEvent;

/**
 * 로그 event 정보에 대한 Type 및 공통 처리를 정의한 추상클래스
 * @author ahnhojung
 *
 * @param <T> event Object
 */
@SuppressWarnings("serial")
public abstract class AbstractLogEvent <T> extends ApplicationEvent{
	/** log send/receive event  */
	private LogEventType type;
	/** default masking character(*) */
	private final byte maskChar = 0x2A;
	/** log masking 여부 */
	private boolean isMasking = false;

	/**
	 * log event constructor
	 * @param type	send/receive
	 * @param source		log source
	 * @param isMasking	log masking 여부
	 */
	protected AbstractLogEvent(LogEventType type, T source, boolean isMasking) {
		super(source);
		this.type = type;
		this.isMasking = isMasking;
	}

	/**
	 * 송/수신 유형 {@link LogEventType}
	 * @return 송/수신 유형
	 */
	public LogEventType getType() {
		return type;
	}

	/**
	 * return log event source
	 * @return log source
	 */
	public abstract T getEventSource();

	/**
	 * log masking 여부
	 * @return	log masking 여부
	 */
	public boolean isMasking() {
		return isMasking;
	}

	/**
	 * default masking character
	 * @return	masking character
	 */
	public byte getMaskChar() {
		return maskChar;
	}
}
