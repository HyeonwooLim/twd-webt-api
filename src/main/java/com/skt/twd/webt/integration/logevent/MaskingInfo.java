package com.skt.twd.webt.integration.logevent;

/**
 * 고정 길이 로그 masking 처리를 위한 masking 위치 정보
 * @author ahnhojung
 *
 */
public class MaskingInfo {
	private String key;
	private int pos;
	private int length;

	/**
	 * byte[] stream 에 대한 masking 처리 위치 정보 constructor
	 * @param key	field key
	 * @param pos	masking start position
	 * @param length		masking length
	 */
	public MaskingInfo(String key, int pos, int length) {
		this.key = key;
		this.pos = pos;
		this.length = length;
	}

	public MaskingInfo(String key) {
		this(key, 0,0);
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}
