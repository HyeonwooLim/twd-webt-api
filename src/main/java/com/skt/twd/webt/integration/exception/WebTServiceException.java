package com.skt.twd.webt.integration.exception;

import com.skt.twd.webt.utils.exceptions.InterfaceClientException;

/**
 * 전문 parsing / build 오류 등 전문 송/수신 시 발생되는 Runtime 오류에 대한 exception
 *
 * <pre>
 * error code define
 *
 * INF0001		webt physical error
 * INF0010		runtime error
 * INF0020		parse/build error
 * </pre>
 * @author ahnhojung
 *
 */
@SuppressWarnings("serial")
public class WebTServiceException extends InterfaceClientException{

	/**
	 * WebTServiceException
	 * @param exceptionCode	errorcode
	 * @param cause	cause
	 */
	public WebTServiceException(String exceptionCode, Throwable cause) {
        super(exceptionCode, InterfaceType.SWING, cause);
    }

	/**
	 * WebTServiceException
	 * @param exceptionCode	error code
	 * @param debugMessage	debug message
	 */
	public WebTServiceException(String exceptionCode, String debugMessage) {
        super(exceptionCode, InterfaceType.SWING);
        setDebugMessage(debugMessage);
        setOrgExceptionCode(exceptionCode);
        setOrgMessage(debugMessage);
    }

	/**
	 * WebTServiceException
	 * @param exceptionCode	error code
	 * @param cause	cause
	 * @param debugMessage	debug message
	 */
    public WebTServiceException(String exceptionCode, Throwable cause, String debugMessage) {
        super(exceptionCode, InterfaceType.SWING, cause);
        setDebugMessage(debugMessage);
        setOrgExceptionCode(exceptionCode);
        setOrgMessage(debugMessage);
    }
}
