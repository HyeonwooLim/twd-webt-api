package com.skt.twd.webt.integration.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.skt.twd.webt.integration.common.WebTBanner;
import com.skt.twd.webt.integration.common.WebTConst;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.common.WebTProperites.WebTChannelProperty;
import com.skt.twd.webt.integration.common.WebTProxyProperties.WebTProxyClientProperties;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.proxy.WebTProxyDTO;

/**
 * 연동 처리 시 proxy 처리를 위한 서비스 처리 클래스
 *
 * @author ahnhojung
 *
 */
public class WebTProxyServiceBean extends AbstractWebTService{

	private static Logger logger = LoggerFactory.getLogger(WebTProxyServiceBean.class);

	@Autowired
	private WebTProperites properties;
	@Autowired
	private WebTProxyClientProperties proxyProperties;

	public WebTProxyServiceBean() {
		logger.info(WebTBanner.getAnsiColor("[WebTService] WebT proxy mode start...", WebTBanner.AnsiColor.ANSI_YELLOW));
	}

	/**
	 * create http client factory
	 * @return http request factory
	 */
	private ClientHttpRequestFactory getClientHttpRequestFactory() {
	    int timeout = proxyProperties.getTimeout();
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
	    clientHttpRequestFactory.setConnectTimeout(timeout);
	    return clientHttpRequestFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ByteArrayInputStream invokeService(WebTServiceInfo serviceInfo, ByteArrayOutputStream bout)
			throws WebTServiceException {

        WebTChannelProperty channelPropety = properties.getChannelPropety(serviceInfo.getChannelName());
        if(channelPropety == null) {
        	throw new  WebTServiceException("INF0010", "[" + serviceInfo.getChannelName() + "] channel name not exist");
        }

		String url = proxyProperties.getUrl();

		ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
		RestTemplate restTemplate = new RestTemplate(requestFactory);

		WebTProxyDTO dto = new WebTProxyDTO();

		dto.setGroupName(StringUtils.defaultIfBlank(serviceInfo.getPoolGroupName(), channelPropety.getPoolGroupName()));
		dto.setServiceId(StringUtils.defaultIfBlank(serviceInfo.getServiceId(), channelPropety.getServiceId()));
		dto.setMessage(bout.toByteArray());

		HttpEntity<WebTProxyDTO> request = new HttpEntity<>(dto);
		WebTProxyDTO foo = restTemplate.postForObject(url + WebTConst.WEBT_DEFAULT_PROXY_URL, request, WebTProxyDTO.class);

		return new ByteArrayInputStream(Optional.ofNullable(foo.getMessage()).orElse(new byte[0]));
	}
}
