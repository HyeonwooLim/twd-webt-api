package com.skt.twd.webt.integration.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import com.skt.twd.webt.integration.common.WebTLogMaskingProperties;
import com.skt.twd.webt.integration.common.WebTProperites;
import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.field.IHeaderField;
import com.skt.twd.webt.integration.handler.IReceiveHeaderHandler;
import com.skt.twd.webt.integration.logevent.LogEventType;
import com.skt.twd.webt.integration.logevent.MaskingInfo;
import com.skt.twd.webt.integration.logevent.notifier.ByteLogEventNotifier;
import com.skt.twd.webt.integration.logevent.notifier.ObjectLogEventNotifier;
import com.skt.twd.webt.integration.processor.annotation.FieldItem;
import com.skt.twd.webt.integration.service.WebTServiceInfo;
import com.skt.twd.webt.integration.service.WebTServiceUtil;

/**
 * WebT 전문 수신 처리 구현체
 *
 * @author ahnhojung
 *
 */
public class DefaultWebTReceiveProcessor implements IWebTReceiveProcessor{
	private static Logger logger = LoggerFactory.getLogger(DefaultWebTReceiveProcessor.class);

	private IReceiveHeaderHandler headerHandler;

	@Autowired
	private WebTProperites properties;
	@Autowired
	private WebTLogMaskingProperties logmaskingProperties;

	// default receive encoding
	private String encoding = "euc-kr";

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receive(final WebTServiceInfo serviceInfo, final ByteArrayInputStream bin, final Object vo) throws WebTServiceException {
		List <Field> memberFieldList = new ArrayList<>();

		Field[] fields = vo.getClass().getDeclaredFields();

		int offset = 0;

		// receive vo logging event
		ObjectLogEventNotifier logObjectEvent = null;
		// receive byte logging event
		ByteLogEventNotifier logStreamEvent = null;
		// log masking field list
		ArrayList<MaskingInfo> maskingList = new ArrayList<>();

		try {
			bin.mark(0);
			byte[] recvBytes = StreamUtils.copyToByteArray(bin);
			bin.reset();
			// byte log event 생성
			logStreamEvent = new ByteLogEventNotifier(LogEventType.RECV_COMPLATE, recvBytes, logmaskingProperties.isEnabled());

			// header field 추출 및 field ordering
			Field headerField = WebTServiceUtil.findHeaderField(fields, memberFieldList);
			Field[] memberField = memberFieldList.toArray(new Field[memberFieldList.size()]);

			// header parsing
			offset = messageParse(offset,
						serviceInfo,
						headerField.get(vo).getClass().getFields(),
						headerField.get(vo),
						bin,
						maskingList);
			// call receive header handler
			if(headerHandler != null) {
				headerHandler.handleHeader((IHeaderField)headerField.get(vo), bin, serviceInfo, properties);
			}
			// body parsing
			offset = messageParse(offset,
						serviceInfo,
						memberField,
						vo,
						bin,
						maskingList);

			// add masking fields list
			logStreamEvent.addMaskingInfo(maskingList);
			logObjectEvent = new ObjectLogEventNotifier(LogEventType.RECV_COMPLATE, vo, logmaskingProperties.isEnabled());

		}catch(IllegalAccessException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IllegalArgumentException e) {
			throw new WebTServiceException("INF0010", e);
		}catch(IOException e) {
			throw new WebTServiceException("INF0010", e);
		}finally {
			// 전문 송수신 byte stream 로그가 정상 오류가 발생 했을 때에도 저장될 수 있도록 finally에 추가
			Optional.ofNullable(logStreamEvent).ifPresent(e -> applicationEventPublisher.publishEvent(e));
			Optional.ofNullable(logObjectEvent).ifPresent(e -> applicationEventPublisher.publishEvent(e));
		}
	}

	/**
	 * field list 정보를 이용하여 수신 byte[] stream을 parsing 및 수신 vo 객체에 값 설정
	 * @param offset 	parsing current offset
	 * @param serviceInfo service info
	 * @param fields receive vo field array
	 * @param vo receive vo
	 * @param bin receive byte stream
	 * @param maskingList masking field
	 * @return  parsing offset
	 * @throws WebTServiceException parsing exception
	 */
	protected int messageParse(int offset, final WebTServiceInfo serviceInfo, Field[] fields, final Object vo,
			final ByteArrayInputStream bin, final ArrayList<MaskingInfo> maskingList) throws WebTServiceException {
		// 반복부의 record count field를 찾기 위한 field key map
		Map<String, Field> keyMap = new HashMap<>();

		// annotation 에 정의된 order로 sort
		fields = WebTServiceUtil.sortOrder(fields);

		for(Field field : fields) {
			field.setAccessible(true);
			FieldItem annotation = field.getDeclaredAnnotationsByType(FieldItem.class)[0];

			int length = annotation.length();
			String cntRef = annotation.cntRef();
			String remarks = annotation.remarks();
			boolean alignRight = annotation.alignRight();
			char padChar = annotation.padChar();

			int fixedCnt = annotation.fixedCnt();

			/*
			 * TODO : byte receive된 field에 기본값이 있는 경우 설정이 필요할까?
			 */
//			String defaultValue = annotation.value();
			String fieldName = field.getName();

			try {
				if(field.getType() == List.class) {	// 반복부 처리
					int recordSize = 0;
					if(StringUtils.hasText(cntRef)) {
						Field cntField = keyMap.get(cntRef.trim());
						if (cntField == null) {
							throw new WebTServiceException("INF0020", "record message parse error. [refcnt=" + cntRef.trim() + "] in [" + field.getName() + "] field does not exist. currnet offset [" + offset + "]");
						}
						try {
							recordSize = Integer.valueOf(String.valueOf(cntField.get(vo)));
						}catch(Exception e) {
							throw new WebTServiceException("INF0020", e, "record message parse error. [refcnt=" + cntRef.trim() + ", value=[" + cntField.get(vo) +"] in [" + field.getName() + "] field value is not valid. currnet offset [" + offset + "]");
						}
					} else if(fixedCnt > 0) {
						try {
							recordSize = fixedCnt;
						}catch(Exception e) {
							throw new WebTServiceException("INF0020", e, "record message parse error. [fixedCnt=" + fixedCnt + " is not valid. currnet offset [" + offset + "]");
						}
					} else {
						String strRecordSize = WebTServiceUtil.getFieldInStream(encoding, bin, length, serviceInfo.isTrim());
						try {
							recordSize = Integer.parseInt(strRecordSize);	offset += length;
						}catch(NumberFormatException e) {
							throw new WebTServiceException("INF0020", e, "record message parse error. record size field [" + fieldName + "] value [" + strRecordSize + "] is not valid. currnet offset [" + offset + "]length[" + length +"]");
						}
					}

					// 반복부 array list 생성
					List<Object> recordList = new ArrayList<>();
					field.setAccessible(true);
					field.set(vo, recordList);
					ParameterizedType type = (ParameterizedType)field.getGenericType();
					Class<?> typeClass = (Class<?>)type.getActualTypeArguments()[0];

					if(logger.isDebugEnabled()) {
						logger.debug("record[{}][{}]length[{}]cntRef[{}]fixedCnt[{}]size[{}]", field.getName(), remarks, length, cntRef, fixedCnt, recordSize);
					}

					for(int i = 0; i < recordSize; i++) {
						Object record;
						try {
							record = typeClass.newInstance();
						} catch (InstantiationException e) {
							throw new WebTServiceException("INF0020", e, "message parse error. can't instantiated record class. [" + typeClass + "] . currnet offset [" + offset + "]");
						}
						// 반복부 record parsing
						messageParse(offset, serviceInfo, record.getClass().getDeclaredFields(), record, bin, maskingList);
						recordList.add(record);
					}
				}else {

					/*
					 * 수신 VO Field의 길이가 음수로 정의된 경우 해당 Field 에 parsing 이후 남은 데이터를 저장처리
					 */
					if( length < 0) {
						byte[] buf = new byte[bin.available()];
						bin.read(buf);
						String value = new String(buf, encoding).trim();

						if(field.getType() == Integer.TYPE) {
							field.set(vo, value.length());
						}else {
							field.set(vo, value);
						}
						offset += length;
						// parsing end
						if(logger.isDebugEnabled()) {
							logger.debug("field[{}][{}]length[{}]cntRef[{}]alignRight[{}]padChar[{}]offset[{}]value[{}]", field.getName(), remarks, length, cntRef, alignRight, padChar, offset, value);
						}

						break;
					}else {
						String value = WebTServiceUtil.getFieldInStream(encoding, bin, length, serviceInfo.isTrim());
						if(logmaskingProperties.getKeyMap().containsKey(fieldName)) {
							maskingList.add(new MaskingInfo(fieldName, offset, length));
						}

						if(field.getType() == Integer.TYPE) {
							field.set(vo, Integer.valueOf(value));
						}else {
							field.set(vo, value);
						}
						offset += length;

						keyMap.put(field.getName(), field);

						if(logger.isDebugEnabled()) {
							logger.debug("field[{}][{}]length[{}]cntRef[{}]alignRight[{}]padChar[{}]offset[{}]value[{}]", field.getName(), remarks, length, cntRef, alignRight, padChar, offset, value);
						}
					}

				}
			}catch(WebTServiceException e) {
				throw e;
			}catch(NumberFormatException e) {
				throw new WebTServiceException("INF0020", e, e.toString() + ". key[" + fieldName + "] length [" + length + "] current offset [" + offset + "]");
			}catch(IllegalAccessException e) {
				throw new WebTServiceException("INF0020", e, e.toString() + ". key[" + fieldName + "] length [" + length + "] current offset [" + offset + "]");
			}catch(IllegalArgumentException e) {
				throw new WebTServiceException("INF0020", e, e.toString() + ". key[" + fieldName + "] length [" + length + "] current offset [" + offset + "]");
			}catch(IOException e) {
				throw new WebTServiceException("INF0020", e, "message parse error. key[" + fieldName + "] length [" + length + "] current offset [" + offset + "]");
			}
		}
		return offset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEncoding(final String encoding) {
		this.encoding = encoding;

	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReceiveHeaderHandler(final IReceiveHeaderHandler handler) {
		this.headerHandler = handler;

	}

	@Override
	public IReceiveHeaderHandler getReceiveHeaderHandler() {
		return this.headerHandler;
	}

	@Override
	public String getEncoding() {
		return this.encoding;
	}
}
