package com.skt.twd.webt.integration.service;

import com.skt.twd.webt.integration.common.WebTConst;

/**
 * webt 거래 호출 에 필요한 정보 ( 채널, 서비스명, TXCODE 등 ) 관리 클래스
 *
 * @author ahnhojung
 *
 */
public class WebTServiceInfo {
	/** webt channel name */
	private String channelName = WebTConst.DEFAULT_CHANNLE_NAME;
	/** webt pool group name */
	private String poolGroupName;
	/** webt service id */
	private String serviceId;
	/** webt tx code */
	private String txCode;
	/** webt channel id */
	private String channelId;
	/** receive data trim */
	private boolean trim = true;

	private WebTServiceInfo() {}

	public static Builder builder() {
		return new Builder();
	}

	public static Builder builder(String channelName) {
		return new Builder(channelName);
	}

	public static class Builder{
		/** webt channel name */
		private String channelName = WebTConst.DEFAULT_CHANNLE_NAME;
		/** webt pool group name */
		private String poolGroupName;
		/** webt service id */
		private String serviceId;
		/** webt tx code */
		private String txCode;
		/** webt channel id */
		private String channelId;
		/** receive data trim */
		private boolean trim = true;

		private Builder() {}

		private Builder(String channelName) {
			this.channelName = channelName;
		}

		/**
		 * set webt pool group name
		 * @param poolGroupName pool group name
		 * @return builder {@link Builder}
		 */
		public Builder setPoolGroupName(String poolGroupName) {
			this.poolGroupName = poolGroupName;
			return this;
		}

		/**
		 * set webt service id
		 * @param serviceId service id
		 * @return builder {@link Builder}
		 */
		public Builder setServiceId(String serviceId) {
			this.serviceId = serviceId;
			return this;
		}

		/**
		 * set webt tx code
		 * @param txCode txcode
		 * @return builder {@link Builder}
		 */
		public Builder setTxCode(String txCode) {
			this.txCode = txCode;
			return this;
		}

		/**
		 * set webt channel id
		 * @param channelId channel id
		 * @return builder {@link Builder}
		 */
		public Builder setChannelId(String channelId) {
			this.channelId = channelId;
			return this;
		}

		/**
		 * set receive value trim
		 * @param trim value trim
		 * @return builder {@link Builder}
		 */
		public Builder setTrim(boolean trim) {
			this.trim = trim;
			return this;
		}

		/**
		 * service information constructor
		 * @return service info {@link WebTServiceInfo}
		 */
		public WebTServiceInfo build() {
			return new WebTServiceInfo(this);
		}
	}

	private WebTServiceInfo(Builder builder) {
		this.channelName = builder.channelName;
		this.poolGroupName = builder.poolGroupName;
		this.channelId = builder.channelId;
		this.serviceId = builder.serviceId;
		this.txCode = builder.txCode;
		this.trim = builder.trim;
	}

	/**
	 * webt channel name
	 * @return channel name
	 */
	public String getChannelName() {
		return channelName;
	}

	/**
	 * webt pool group name
	 * @return pool group name
	 */
	public String getPoolGroupName() {
		return poolGroupName;
	}

	/**
	 * webt service id
	 * @return service id
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * webt txcode
	 * @return txcode
	 */
	public String getTxCode() {
		return txCode;
	}

	/**
	 * webt channel id
	 * @return channel id
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * value trim
	 * @return value trim
	 */
	public boolean isTrim() {
		return trim;
	}

	@Override
	public String toString() {
		return "MCGServiceInfo [channelName-" + channelName + ", poolGroupName=" + poolGroupName + ", serviceId=" + serviceId + ", txCode=" + txCode
				+ ", channelId=" + channelId + ", trim=" + trim + "]";
	}
}
