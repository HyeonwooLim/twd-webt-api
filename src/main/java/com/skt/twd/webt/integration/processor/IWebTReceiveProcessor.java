package com.skt.twd.webt.integration.processor;

import java.io.ByteArrayInputStream;

import com.skt.twd.webt.integration.exception.WebTServiceException;
import com.skt.twd.webt.integration.handler.IReceiveHeaderHandler;
import com.skt.twd.webt.integration.service.WebTServiceInfo;

/**
 * 전문 수신 처리 interface
 *
 * @author ahnhojung
 *
 */
public interface IWebTReceiveProcessor{
	/**
	 * webt 전문 수신 처리
	 * @param serviceInfo  service info {@link WebTServiceInfo}
	 * @param bin receive byte[] stream
	 * @param vo receive vo
	 * @throws WebTServiceException parsing exception
	 */
	void receive(WebTServiceInfo serviceInfo, ByteArrayInputStream bin, Object vo) throws WebTServiceException;

	/**
	 * 수신 header hander 설정
	 * @param handler receive header handler
	 */
	void setReceiveHeaderHandler(IReceiveHeaderHandler handler);

	/**
	 * 수신 header handler
	 * @return receive header handler
	 */
	IReceiveHeaderHandler getReceiveHeaderHandler();

	/**
	 * 수신 encoding 설정
	 * @param encoding encoding
	 */
	void setEncoding(String encoding);

	/**
	 * 수신 encoding
	 * @return encoding
	 */
	String getEncoding();
}
