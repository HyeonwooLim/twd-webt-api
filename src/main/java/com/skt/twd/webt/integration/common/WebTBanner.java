package com.skt.twd.webt.integration.common;

/**
 * WebT Integration console log decoration
 *
 * @author ahnhojung
 *
 */

public class WebTBanner {
	public static enum AnsiColor{
		ANSI_DEFAULT("\033[39m"),
		ANSI_BRIGHT_RED("\033[91m"),
		ANSI_BRIGHT_YELLOW("\033[93m"),
		ANSI_RED("\u001B[31m"),
		ANSI_YELLOW("\u001B[33m"),
		ANSI_BLUE("\u001B[34m"),
		ANSI_GREEN("\033[32m"),
		ANSI_CYAN("\u001B[36m"),
		ANSI_BLINK("\u001B[5m"),
		ANSI_RAPID_BLINK("\u001B[6m"),
		ANSI_RESET("\033[0;39m");

		private String color;
		AnsiColor(String color) { this.color = color; }
		public String getColor() {
			return this.color;
		}
	}

	/**
	 * 입력된 메시지 decoration을 위한 Ansi Character 처리
	 * @param message	log message
	 * @param colors		log color
	 * @return decorated message
	 */
	public static String getAnsiColor(String message, AnsiColor ...colors ) {
		StringBuilder builder = new StringBuilder();
		for(AnsiColor color : colors) {
			builder.append(color.getColor());
		}
		builder.append(message);
		builder.append(AnsiColor.ANSI_RESET.getColor());
		return builder.toString();
	}

//	public static void main(String[] args) {
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_BRIGHT_RED));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_BRIGHT_YELLOW));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_RED));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_YELLOW));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_BLUE));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_GREEN));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_CYAN));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_BLINK));
//		System.out.println(getAnsiColor("Hello World", AnsiColor.ANSI_RAPID_BLINK, AnsiColor.ANSI_RED));
//	}
}
